import React, { Component } from "react";
import { observable, action } from "mobx";
import { SESSION_ACCESS_TOKEN, TOKEN, SESSION_USER } from "./Types";
import { AsyncStorage } from "react-native";
import { SocialStore } from "./SocialStore";
import DrawerNavigator from "../src/screens/drawer";
import { Auth } from "./Auth";

export const API_URL = "http://54.207.43.173:4730/api/v1";

export const ProfileStore = observable.object(
  {
    userProfile: [],
    myPosts: 0,
    modalEdit: false,
    loading: false,
    city: "",
    cityId: 0,
    state: "",
    stateId: 0,
    country: "",
    baseImage: "",
    getUserProfile: async function() {
      return await AsyncStorage.getItem(SESSION_USER).then(dados => {
        const next = JSON.parse(dados);
        const iduser = next.profile.id;
        const options = {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            api_token: next.token
          }
        };
        fetch(`${API_URL}/resources/social/people/${iduser}`, options)
          .then(res => {
            return res.json();
          })
          .then(data => {
            // console.log("DATA USER PROFILE", data);

            this.setUserProfile(data);
            //this.getLocation(data.city_id, data.state_id, next.token);
          })
          .catch(err => {
            console.log("Deu erro", err);
          });
      });
    },
    getCity: function(cityid, idtoken) {
      const options = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: idtoken
        }
      };
      fetch(`${API_URL}/resources/general/cities/${cityid}`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          //console.log("DATA CITY", data);
          this.setCity(data.name);
        })
        .catch(err => {
          console.log("Deu erro", err);
        });
    },
    getState: function(stateid, idtoken) {
      // console.log("entrei no get state");
      const options = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: idtoken
        }
      };
      fetch(`${API_URL}/resources/general/states/${stateid}`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          // console.log("DATA STATE", data);
          this.setState(data.name);
        })
        .catch(err => {
          console.log("Deu erro", err);
        });
    },
    getCountry: function(countryid, idtoken) {
      const options = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: idtoken
        }
      };
      fetch(`${API_URL}/resources/general/countries/${countryid}`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          //console.log("DATA country", data);
          this.setCountry(data.name);
        })
        .catch(err => {
          console.log("Deu erro", err);
        });
    },
    updateProfile: async function(
      name,
      birth_date,
      address,
      address2,
      zip_code,
      country_id,
      stateName,
      cityName,
      gender,
      biography,
      interests,
      activities,
      music,
      books,
      cinema,
      television,
      callback
    ) {
      try {
        let userSession = await AsyncStorage.getItem(SESSION_USER);
        let userData = JSON.parse(userSession);
        const iduser = userData.profile.id;
        let city_id = 1;
        let state_id = 1;
        let success = false;
        let updateImg = true;
        let error = "";

        const options = {
          method: "PUT",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            api_token: userData.token
          },
          body: JSON.stringify({
            name,
            birth_date,
            address,
            address2,
            zip_code,
            country_id,
            state_id,
            city_id,
            gender,
            biography,
            interests,
            activities,
            music,
            books,
            cinema,
            television
          })
        };

        let response = await fetch(
          `${API_URL}/resources/social/people/${iduser}`,
          options
        );
        let result = await response.json();

        if (!result) {
          error = "não foi possível atualizar o perfil";
          return { success, error };
        }

        userData.profile.name = name;
        AsyncStorage.setItem(SESSION_USER, JSON.stringify(userData));
        Auth.setName(name);

        if (this.baseImage != "") {
          updateImg = await this.updateProfilePhoto();
        }

        if (this.baseImage != "" && !updateImg) {
          error = "não foi possível atualizar a foto do perfil.";
          return { success, error };
        }
        success = true;
        this.setUserProfile(result);

        return { success, error };
      } catch (err) {
        let error = "não foi possível atualizar o perfil.";
        let success = false;
        return { success, error };
      }
    },
    updateProfilePhoto: async function() {
      return await AsyncStorage.getItem(SESSION_USER).then(data => {
        let next = JSON.parse(data);
        let token = next.token;
        let profileid = next.profile.id;
        if (next.token != null) {
          let asset_content_base64 = this.baseImage;
          let asset_file_name = "some_image.jpg";
          const options = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              api_token: token
            },
            body: JSON.stringify({ asset_file_name, asset_content_base64 })
          };
          fetch(`${API_URL}/socials/profiles/${profileid}/asset`, options)
            .then(res => {
              //console.log("RESULTADO", res.status);
              return res.json();
            })
            .then(data => {
              let photourl = data.Location;
              let cuturl = photourl.replace(
                "https://fc-exclusive.s3.sa-east-1.amazonaws.com/uploads/social/profiles/",
                ""
              );
              next.thumb_url = cuturl;
              AsyncStorage.setItem(SESSION_USER, JSON.stringify(next));
              Auth.setProfilePhoto(data.Location);
            })
            .catch(err => {
              console.log("err", err);
            });
        }
      });
    },
    getNumberCity: function(cityname, token, callback) {
      const options = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: token
        }
      };
      fetch(`${API_URL}/resources/general/cities?name=${cityname}`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          this.setCityID(data[0].id);
          this.setStateID(data[0].state_id);
          callback(true);
        })
        .catch(err => {
          callback(false);
          console.log("Deu erro", err);
        });
    },
    getLocation: function(cityId, stateId, token) {
      if (cityId != null) {
        this.getCity(cityId, token);
      }
      if (stateId != null) {
        this.getState(stateId, token);
      }

      this.setCountry("Brasil");
    },
    setUserProfile: function(data) {
      return (this.userProfile = data);
    },
    setMyPosts: function(data) {
      return (this.myPosts = data);
    },
    setModalEdit: function(data) {
      return (this.modalEdit = data);
    },
    setLoading: function(data) {
      return (this.loading = data);
    },
    setCity: function(data) {
      return (this.city = data);
    },
    setState: function(data) {
      return (this.state = data);
    },
    setCountry: function(data) {
      return (this.country = data);
    },
    setBaseImage: function(data) {
      return (this.baseImage = data);
    },
    setCityID: function(data) {
      return (this.cityId = data);
    },
    setStateID: function(data) {
      return (this.stateId = data);
    }
  },
  {
    //Campos
    userProfile: observable,
    myPosts: observable,
    modalEdit: observable,
    loading: observable,
    city: observable,
    state: observable,
    country: observable,
    baseImage: observable,
    cityId: observable,
    stateId: observable,

    //setters
    setUserProfile: action,
    setMyPosts: action,
    setModalEdit: action,
    setLoading: action,
    setCity: action,
    setState: action,
    setCountry: action,
    setBaseImage: action,
    setCityID: action,
    setStateID: action,

    //actions
    getUserProfile: action,
    updateProfile: action,
    getCity: action,
    getState: action,
    getCountry: action,
    getNumberCity: action,
    getLocation: action
  }
);
