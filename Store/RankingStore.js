import React, { Component } from "react";
import { observable, action } from "mobx";
import { SESSION_ACCESS_TOKEN, TOKEN, SESSION_USER } from "./Types";
import { AsyncStorage } from "react-native";
import { Auth } from "./Auth";

export const API_URL = "http://54.207.43.173:4730/api/v1";

export const RankingStore = observable.object(
  {
    ranking: [],
    getRanking: function(callback) {
      AsyncStorage.getItem(SESSION_USER).then(res => {
        let next = JSON.parse(res);
        if (next.token != null) {
          const options = {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              api_token: next.token
            }
          };
          fetch(`${API_URL}/ranking/${Auth.defaultInstance}`, options)
            .then(res => {
              return res.json();
            })
            .then(data => {
              //console.log("ranking ", data.ranking);
              this.setRanking(data.ranking);
              return callback(true);
            })
            .catch(err => {
              console.log(err);
              return callback(false);
            });
        }
      });
    },
    setRanking: function(data) {
      return (this.ranking = data);
    }
  },
  {
    //Campos
    ranking: observable,

    //setters
    setRanking: action,

    //actions
    getRanking: action
  }
);
