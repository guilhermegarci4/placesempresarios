import { observable, action } from "mobx";

export const API_URL = "http://54.207.43.173:4730/api/v1";

export const PortalStore = observable.object(
  {
    loading: true,
    setLoading: function(data) {
      return (this.loading = data);
    }
  },
  {
    loading: observable,

    setLoading: action
  }
);
