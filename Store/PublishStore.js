import React, { Component } from "react";
import { observable, action } from "mobx";
import { SESSION_ACCESS_TOKEN, TOKEN, SESSION_USER } from "./Types";
import { AsyncStorage, Alert } from "react-native";
import { SocialStore } from "./SocialStore";
import DrawerNavigator from "../src/screens/drawer";
import { Auth } from "./Auth";

export const API_URL = "http://54.207.43.173:4730/api/v1";

export const PublishStore = observable.object(
  {
    token: null,
    textPost: "",
    loading: false,
    img: null,
    publish: async function() {
      try {
        let session = await AsyncStorage.getItem(SESSION_USER);
        let dados = JSON.parse(session);
        console.log("ENTREI NA FUNCAO DE PUBLICACAO");
        let publication = {
          message: this.textPost,
          user_id: dados.id,
          username: dados.username,
          name: dados.profile.name,
          profile_id: dados.profile.profile_id,
          thumb_url: dados.thumb_url,
          asset_id: null,
          asset_url: null
        };

        console.log("JSON PUBLICACAO", publication);

        const options = {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            api_token: dados.token
          },
          body: JSON.stringify(publication)
        };

        let sendPub = await fetch(
          `${API_URL}/social/${Auth.defaultInstance}`,
          options
        );
        console.log("RESULTADO DE RESPONSE", sendPub);
        let response = await sendPub.json();

        if (response.success) {
          console.log("ENTREI NO GET POSTS");
          this.setTextPost("");
          SocialStore.getPosts();
          SocialStore.getNotification();
        }
      } catch (e) {
        console.log("DEU RUIM NO PUBLISH", e);
        setTimeout(() => {
          Alert.alert("Erro", "Verifique sua conexão com a internet.", [
            { text: "Ok", onPress: () => console.log("fechou") }
          ]);
        }, 300);
      }
    },
    publishImage: async function() {
      try {
        let session = await AsyncStorage.getItem(SESSION_USER);
        let dados = JSON.parse(session);

        let data = {
          asset_file_name: "some_image.jpg",
          asset_content_base64: this.img
        };

        const options = {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            api_token: dados.token
          },
          body: JSON.stringify(data)
        };
        let sendImage = await fetch(
          `${API_URL}/socials/publications/asset`,
          options
        );
        let result = await sendImage.json();

        let publication = {
          message: this.textPost,
          user_id: dados.id,
          username: dados.username,
          name: dados.profile.name,
          profile_id: dados.profile.profile_id,
          thumb_url: dados.thumb_url,
          asset_id: result.id,
          asset_url: null
        };

        const dataPub = {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            api_token: dados.token
          },
          body: JSON.stringify(publication)
        };

        let sendPub = await fetch(
          `${API_URL}/social/${Auth.defaultInstance}`,
          dataPub
        );
        let response = await sendPub.json();

        if (response.success) {
          this.setTextPost("");
          this.setImg(null);

          SocialStore.getPosts();
          SocialStore.getNotification();
        }
      } catch (e) {
        console.log("DEU RUIM", e);
        setTimeout(() => {
          Alert.alert("Erro", "Verifique sua conexão com a internet.", [
            { text: "Ok", onPress: () => console.log("fechou") }
          ]);
        }, 2000);
      }
    },
    setImg: function(data) {
      return (this.img = data);
    },
    setToken: function(accesstoken) {
      return (this.token = accesstoken);
    },
    setTextPost: function(txt) {
      return (this.textPost = txt);
    },
    setLoading: function(status) {
      return (this.loading = status);
    }
  },
  {
    //Campos
    token: observable,
    textPost: observable,
    loading: observable,
    img: observable,

    //setters
    setToken: action,
    setTextPost: action,
    setLoading: action,
    setImg: action,

    //actions
    publish: action,
    publishImage: action
  }
);
