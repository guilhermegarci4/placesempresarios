import React, { Component } from "react";
import { observable, action } from "mobx";
import { SESSION_ACCESS_TOKEN, TOKEN, SESSION_USER } from "./Types";
import { AsyncStorage } from "react-native";
import { Auth } from "./Auth";

export const API_URL = "http://54.207.43.173:4730/api/v1";

export const VisitProfileStore = observable.object(
  {
    dataProfile: {},
    birthDate: "",
    city: "",
    state: "",
    country: "",
    init: false,
    getDataProfile: function(iduser) {
      AsyncStorage.getItem(SESSION_USER).then(res => {
        let next = JSON.parse(res);
        if (next.token != null) {
          const options = {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              api_token: next.token
            }
          };

          fetch(`${API_URL}/resources/social/people/${iduser}`, options)
            .then(res => {
              return res.json();
            })
            .then(data => {
              //console.log("DADOS DO USUARIO VISITADO ", data);
              if (data.birth_date != null) {
                let day = data.birth_date.substring(8, 10);
                let month = data.birth_date.substring(5, 7);
                let year = data.birth_date.substring(0, 4);

                let date = `${day}/${month}/${year}`;
                this.setBirthDate(date);
              }

              if (data.city_id != null) {
                this.getCity(data.city_id, next.token);
              }

              if (data.state_id != null) {
                this.getState(data.state_id, next.token);
              }

              if (data.country_id != null) {
                this.getCountry(data.country_id, next.token);
              }

              this.setDataProfile(data);
              this.setInit(true);
            })
            .catch(err => {
              console.log(err);
            });
        }
      });
    },
    getCity: function(cityid, idtoken) {
      const options = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: idtoken
        }
      };
      fetch(`${API_URL}/resources/general/cities/${cityid}`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          //console.log("DATA CITY", data);
          this.setCity(data.name);
        })
        .catch(err => {
          // console.log("Deu erro", err);
        });
    },
    getState: function(stateid, idtoken) {
      //console.log("entrei no get state");
      const options = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: idtoken
        }
      };
      fetch(`${API_URL}/resources/general/states/${stateid}`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          //console.log("DATA STATE", data);
          this.setState(data.name);
        })
        .catch(err => {
          console.log("Deu erro", err);
        });
    },
    getCountry: function(countryid, idtoken) {
      const options = {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: idtoken
        }
      };
      fetch(`${API_URL}/resources/general/countries/${countryid}`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          console.log("DATA country", data);
          this.setCountry(data.name);
        })
        .catch(err => {
          console.log("Deu erro", err);
        });
    },
    setDataProfile: function(data) {
      return (this.dataProfile = data);
    },
    setBirthDate: function(data) {
      return (this.birthDate = data);
    },
    setInit: function(data) {
      return (this.init = data);
    }
  },
  {
    //Campos
    dataProfile: observable,
    birthDate: observable,
    init: observable,

    //setters
    setDataProfile: action,
    setBirthDate: action,
    setInit: action,

    //actions
    getDataProfile: action,
    getCity: action,
    getState: action,
    getCountry: action
  }
);
