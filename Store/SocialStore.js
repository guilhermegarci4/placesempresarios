import { observable, action } from "mobx";
import { Auth, API_URL } from "./Auth";
import { Alert } from "react-native";
import { RankingStore } from "./RankingStore";
import { AsyncStorage } from "react-native";
import { SESSION_USER } from "./Types";
import Pusher from "pusher-js/react-native";
import md5 from "md5";
import moment from "moment-timezone";
import { ProfileStore } from "./ProfileStore";

const headers = () => {
  return {
    Accept: "application/json",
    "Content-Type": "application/json",
    api_token: Auth.token
  };
};

export const SocialStore = observable.object(
  {
    textCommentPost: "",
    instanceId: "",
    badgeNotification: 0,
    giftQuantity: "",
    init: false,
    initComments: true,
    loading: false,
    modalInstance: false,
    modalNotification: false,
    dataPosts: [],
    dataComments: [],
    notifications: [],
    listStoryNotification: [],
    inHighPosts: [],

    getNotification: function() {
      AsyncStorage.getItem(SESSION_USER).then(res => {
        const next = JSON.parse(res);
        const iduser = next.id;
        const dateformated = moment
          .tz(next.created_at, "America/Sao_Paulo")
          .format("YYYY-MM-DD HH:mm:ss ZZ");
        const actual = md5(`${iduser}${dateformated}${Auth.defaultInstance}`);

        const config = {
          appId: "31637",
          key: "61432e2bad860249e994",
          secret: "086fc912bff6e1c249a1",
          cluster: "mt1",
          encrypted: true,
          restServer: "http://54.207.43.173:4730/api/v1"
        };

        const pusher = new Pusher("61432e2bad860249e994", config);

        const chatChannel = pusher.subscribe(actual);

        chatChannel.bind("pusher:subscription_succeeded", () => {
          chatChannel.bind("load_new_notification", data => {
            this.getStoryNotification();
            this.setNotification(data);
          });
        });
      });
    },
    getPosts: async function() {
      const options = {
        method: "GET",
        headers: headers()
      };
      await fetch(`${API_URL}/social/${Auth.defaultInstance}/80/0`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          //console.log("ENTREI AQUI GET POSTS", data);

          if (data.error) {
            return Auth.RenewSession();
          }

          if (data === []) {
            this.setLoading(false);
          }
          //this.setDataPosts([]);
          this.getStoryNotification();
          this.setDataPosts(data);
          this.setInit(true);
          let inhigh = data;
          inhigh.sort(function(a, b) {
            return b.gifts.length - a.gifts.length;
          });
          let countposts = 0;
          data.find(obj => {
            if (obj.user_id === Auth.id) {
              countposts += 1;
            }
          });
          ProfileStore.setMyPosts(countposts);
          //this.setInHigh([]);
          this.setInHigh(inhigh);
        })
        .catch(err => {
          console.log("Deu erro", err);
        });
    },
    getStoryNotification: async function() {
      try {
        let userSession = await AsyncStorage.getItem(SESSION_USER);
        let userData = JSON.parse(userSession);
        let success = true;
        let error = "";

        const options = {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            api_token: userData.token
          }
        };

        let response = await fetch(
          `${API_URL}/resources/system/notifications?instance_id=${
            Auth.defaultInstance
          }&user_id=${iduser}&origin_type=Social::Publication&sort=-created_at`,
          options
        );
        let result = await response.json();

        this.setListStory(result);
        this.setBadgeNotification(0);
        result.forEach(item => {
          if (!item.read && item.origin.author.username != Auth.name) {
            this.setBadgeNotification(this.badgeNotification + 1);
          }
        });

        return { success, error };
      } catch (err) {
        let error = "não foi possível excluir a publicação.";
        let success = false;
        return { success, error };
      }
    },
    readNotification: async function(notificationId) {
      let read = true;
      const options = {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          api_token: Auth.token
        },
        body: JSON.stringify({ read })
      };
      await fetch(
        `${API_URL}/resources/system/notifications/${notificationId}`,
        options
      )
        .then(res => {
          return res.json();
        })
        .then(data => {
          const { read } = data;
          //console.log("DEU BOM A BAGACA DO READ");

          if (read) {
            this.getStoryNotification();
          }
        })
        .catch(err => {});
    },
    getComments: async function(pubId, callback) {
      const options = {
        method: "GET",
        headers: headers()
      };
      await fetch(
        `${API_URL}/social/${
          Auth.defaultInstance
        }/publications/${pubId}/comments`,
        options
      )
        .then(r => {
          return r.json();
        })
        .then(response => {
          let answers = response;
          answers.sort(function(a, b) {
            return a.id - b.id;
          });

          this.setDataComments(answers);
          this.setInitComments(false);
          callback(response);
        })
        .catch(err => {
          this.setInitComments(false);
          callback(err);
          console.log("ERRO GET COMMENTS", err);
        });
    },
    postComment: async function(pubId) {
      let id = Auth.id;
      let message = this.textCommentPost;
      let publication_id = pubId;
      let username = Auth.username;
      let name = Auth.name;
      let thumb_url = Auth.ProfilePhoto || null;
      let created_at = null;
      let gifts = [];

      let data = {
        id,
        message,
        publication_id,
        username,
        name,
        thumb_url,
        created_at,
        gifts
      };
      const response = await fetch(
        `${API_URL}/social/${
          Auth.defaultInstance
        }/publications/${pubId}/comments`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            api_token: Auth.token
          },
          body: JSON.stringify(data)
        }
      );
      this.setInitComments(false);
      const result = await response.json();

      if (result.success) {
        this.setTextCommentPost("");
        this.getComments(pubId, () => {
          setTimeout(() => {
            Alert.alert(
              "Sucesso",
              "O seu comentário foi publicado com sucesso!",
              [{ text: "Ok", onPress: () => console.log("fechou") }]
            );
          }, 300);
        });
      }
      return result;
    },
    changeInstance: function(instanceId) {
      //console.log("ENTREI NO CHANGE INSTANCE");
      this.setLoading(true);
      Auth.setDefaultInstance(instanceId);
      //this.setInit(false);
      this.getPosts();
      this.getNotification();
      RankingStore.setRanking([]);
      RankingStore.getRanking(() => {});
      this.setLoading(false);
    },
    postGift: async function(pubId) {
      try {
        const options = {
          method: "GET",
          headers: headers()
        };
        await fetch(
          `${API_URL}/social/${
            Auth.defaultInstance
          }/publications/${pubId}/gift`,
          options
        )
          .then(r => {
            return r.json();
          })
          .then(response => {
            //callback(response);
          })
          .catch(err => {
            //callback(err);
            console.log("ERRO GET gift", err);
          });
      } catch (e) {
        console.log("deum ruim no like", e);
      }
    },
    deletePublish: async function(pubId) {
      console.log("PUBID", pubId);
      try {
        let userSession = await AsyncStorage.getItem(SESSION_USER);
        let userData = JSON.parse(userSession);
        let success = true;
        let error = "";

        const options = {
          method: "DELETE",
          headers: {
            api_token: userData.token
          }
        };

        let response = await fetch(
          `${API_URL}/social/${Auth.defaultInstance}/publications/${pubId}`,
          options
        );
        let result = await response.json();
        console.log("RESULT", result);

        if (!result) {
          error = "não foi possível excluir a publicação";
          return { success, error };
        }
        let getPosts = await this.getPosts();
        return { success, error };
      } catch (err) {
        let error = "não foi possível excluir a publicação.";
        let success = false;
        return { success, error };
      }
    },
    InHigh: function() {
      console.log("nao ha nada aqui");
    },
    setInHigh: function(data) {
      return (this.inHighPosts = data);
    },
    setNotification: function(data) {
      return (this.notifications = data);
    },
    setDataComments: function(data) {
      return (this.dataComments = data);
    },
    setDataPosts: function(data) {
      return (this.dataPosts = data);
    },
    setInit: function(data) {
      return (this.init = data);
    },
    setInitComments: function(data) {
      return (this.initComments = data);
    },
    setTextCommentPost: function(txt) {
      return (this.textCommentPost = txt);
    },
    setModalInstance: function(status) {
      return (this.modalInstance = status);
    },
    setModalNotification: function(status) {
      return (this.modalNotification = status);
    },
    setInstanceId: function(num) {
      return (this.instanceId = num);
    },
    setLoading: function(status) {
      return (this.loading = status);
    },
    setListStory: function(data) {
      return (this.listStoryNotification = data);
    },
    setBadgeNotification: function(data) {
      return (this.badgeNotification = data);
    },
    setGiftQuantity: function(data) {
      return (this.giftQuantity = data);
    }
  },
  {
    //Campos
    dataPosts: observable,
    init: observable,
    initComments: observable,
    textCommentPost: observable,
    modalInstance: observable,
    instanceId: observable,
    loading: observable,
    modalNotification: observable,
    listStoryNotification: observable,
    badgeNotification: observable,
    inHighPosts: observable,
    giftQuantity: observable,

    //setters
    setModalNotification: action,
    setDataPosts: action,
    setInit: action,
    setInitComments: action,
    setTextCommentPost: action,
    setModalInstance: action,
    changeInstance: action,
    setInstanceId: action,
    setLoading: action,
    setListStory: action,
    setBadgeNotification: action,
    setInHigh: action,

    //actions
    getPosts: action,
    getComments: action,
    postGift: action,
    getNotification: action,
    getStoryNotification: action,
    InHigh: action
  }
);
