import React, { Component } from "react";
import { observable, action } from "mobx";
import { SESSION_ACCESS_TOKEN, TOKEN, SESSION_USER } from "./Types";
import { AsyncStorage } from "react-native";
import { SocialStore } from "./SocialStore";
import { RankingStore } from "./RankingStore";
import { Notifications, Permissions, Location } from "expo";
import { ProfileStore } from "./ProfileStore";

export const API_URL = "http://54.207.43.173:4730/api/v1";
const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

const headers = () => {
  return {
    Accept: "application/json",
    "Content-Type": "application/json"
  };
};

export const Auth = observable.object(
  {
    id: "",
    username: "",
    name: "",
    loading: false,
    loggedIn: false,
    token: "",
    defaultInstance: 438,
    ProfilePhoto: "",
    instances: [],
    signIn: async function(username, password, callback) {
      this.setLoading(true);
      const options = {
        method: "POST",
        headers: headers(),
        body: JSON.stringify({ username, password })
      };
      await fetch(`${API_URL}/user/sign_in`, options)
        .then(res => {
          return res.json();
        })
        .then(data => {
          const success = true;
          if (data.error) {
            callback(data.error);
          } else {
            //console.log("ENTREI NO FETCH DO SIGN IN");
            const { token, id, username, profile, instances } = data;
            this.dataSets(
              id,
              token,
              data.thumb_url,
              profile.name,
              username,
              instances
            );
            let canEnter = false;

            instances.find(obj => {
              if (obj.id === 438) {
                canEnter = true;
              }
            });

            if (canEnter) {
              AsyncStorage.setItem(SESSION_ACCESS_TOKEN, token);
              AsyncStorage.setItem(SESSION_USER, JSON.stringify(data));
              SocialStore.getPosts();
              ProfileStore.getUserProfile();
              SocialStore.getNotification();
              RankingStore.getRanking(() => {});
              this.savePushToken(token);
              callback(success);
            } else {
              callback("Você não faz parte dessa plataforma.");
            }
          }
        })
        .catch(err => {
          this.setLoading(false);
          callback(err);
        });
    },
    isSignedIn: async function() {
      return await AsyncStorage.getItem(SESSION_USER)
        .then(res => {
          if (res !== null) {
            let data = JSON.parse(res);
            this.dataSets(
              data.id,
              data.token,
              data.thumb_url,
              data.profile.name,
              data.username,
              data.instances
            );

            SocialStore.getPosts();
            ProfileStore.getUserProfile();
            SocialStore.getNotification();
            RankingStore.getRanking(() => {});
            const success = true;
            return success;
          }
          const failed = false;
          return failed;
        })
        .catch(err => {
          return err;
        });
    },
    RenewSession: async function() {
      return await AsyncStorage.getItem(SESSION_USER)
        .then(res => {
          let next = JSON.parse(res);
          let idtoken = next.id;
          if (res !== null) {
            const options = {
              method: "GET",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
              }
            };
            fetch(`${API_URL}/user/renew/${idtoken}`, options)
              .then(res => {
                return res.json();
              })
              .then(data => {
                const { token, id, username, profile, instances } = data;
                this.dataSets(
                  id,
                  token,
                  data.thumb_url,
                  profile.name,
                  username,
                  instances
                );

                AsyncStorage.setItem(SESSION_ACCESS_TOKEN, token);
                AsyncStorage.setItem(SESSION_USER, JSON.stringify(data));
                SocialStore.getPosts();
                ProfileStore.getUserProfile();
                SocialStore.getNotification();
                this.savePushToken(token);
              })
              .catch(err => {
                this.setLoading(false);
                callback(err);
              });
          }
        })
        .catch(err => {
          return err;
        });
    },
    dataSets: function(id, token, photo, name, username, instances) {
      this.setId(id);
      this.setToken(token);
      if (photo != null) {
        this.setProfilePhoto(`${url}${photo}`);
      } else {
        this.setProfilePhoto(photo);
      }

      this.setName(name);
      this.setUsername(username);
      this.setInstances(instances);
    },
    setInstances: function(data) {
      return (this.instances = data);
    },
    setUsername: function(txt) {
      return (this.username = txt);
    },
    setName: function(txt) {
      return (this.name = txt);
    },
    setId: function(id) {
      return (this.id = id);
    },
    setProfilePhoto: function(photo) {
      return (this.ProfilePhoto = photo);
    },
    setToken: function(token) {
      return (this.token = token);
    },
    setDefaultInstance: function(di) {
      return (this.defaultInstance = di);
    },
    setLoginStatus: function(txt) {
      return (this.loggedIn = txt);
    },
    setLoading: function(info) {
      return (this.loading = info);
    },
    savePushToken: function(apiToken) {
      _registerForPushNotificationsAsync().then(push_notification_token => {
        if (push_notification_token != null) {
          const options = {
            method: "POST",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              api_token: apiToken
            },
            body: JSON.stringify({ push_notification_token })
          };
          fetch(`${API_URL}/user/push_notification_token`, options)
            .then(res => {
              return res.json();
            })
            .then(data => {});
        }
      });
    }
  },
  {
    //Campos
    name: observable,
    username: observable,
    id: observable,
    loading: observable,
    loggedIn: observable,
    ProfilePhoto: observable,

    //setters
    setLoading: action,
    setToken: action,
    setProfilePhoto: action,
    setId: action,
    setUsername: action,
    setName: action,
    dataSets: action,

    //actions
    signIn: action,
    isSignedIn: action
  }
);

async function _registerForPushNotificationsAsync() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;

  if (existingStatus !== "granted") {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }

  if (finalStatus !== "granted") {
    return 0;
  }

  let token = await Notifications.getExpoPushTokenAsync();

  return token;
}
