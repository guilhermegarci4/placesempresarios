import React, { Component } from "react";
import { View, Modal, StyleSheet, ActivityIndicator, Platform } from "react-native";

export default class Loading extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Modal
        animationType="none"
        transparent
        visible={this.props.visible}
        onRequestClose={() => {}}
      >
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#0d3e59" />
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    position: "absolute",
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#000",
    opacity: 0.5
  }
});
