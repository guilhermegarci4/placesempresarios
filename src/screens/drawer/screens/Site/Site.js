import React, { Component, PureComponent } from "react";
import { View, StyleSheet, Platform, WebView, Image } from "react-native";
import { Container, Header, Left, Right, Body, Button } from "native-base";
import { Constants } from "expo";
import { MaterialIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import Loading from "../../../../components/loading";

@observer
export default class Site extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
    window.globalNavigator = this.props.navigation;
    this.ShowPortal = this.ShowPortal.bind(this);
    this.OpenDrawer = this.OpenDrawer.bind(this);
    //this.LoadError = this.LoadError.bind(this);
  }

  ShowPortal() {
    this.setState({ loading: false });
  }

  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#rgb(232,9,17)" }}>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Loading visible={this.state.loading} />
        <Header style={{ backgroundColor: "#rgb(232,9,17)" }}>
          <Left>
            <Button transparent onPress={this.OpenDrawer}>
              <MaterialIcons name="dehaze" size={25} color="#fafafa" />
            </Button>
          </Left>
          <Body
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginLeft: Platform.OS === "ios" ? 0 : "21%"
            }}
          >
            <Image
              source={require("../../../../../assets/img/logoFarmaponte.png")}
              style={{
                width: 100,
                height: 35
              }}
              resizeMode="contain"
            />
          </Body>
          <Right />
        </Header>
        <WebView
          source={{ uri: "http://www.farmaponte.com.br/" }}
          onLoad={this.ShowPortal}
        />
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#rgb(232,9,17)",
    height: Constants.statusBarHeight
  }
});
