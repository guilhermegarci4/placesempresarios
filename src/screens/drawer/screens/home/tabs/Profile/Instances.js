import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon,
  List,
  ListItem,
  Button
} from "native-base";
import { observer } from "mobx-react";
import { Auth } from "../../../../../../../Store/Auth";
import { SocialStore } from "../../../../../../../Store/SocialStore";

@observer
export default class Instances extends Component {
  constructor(props) {
    super(props);
    this.state = { instance: "" };
  }

  RenderInstances(item) {
    if (SocialStore.instanceId == item.id) {
      return (
        <ListItem
          button
          onPress={() => {
            SocialStore.setInstanceId(item.id);
            SocialStore.setDataPosts([]);
            SocialStore.changeInstance(item.id);
            SocialStore.setModalInstance(false);
          }}
        >
          <Body>
            <Text style={{ color: "tomato" }}>{item.name}</Text>
          </Body>
          <Right>
            <Icon name="md-checkmark" style={{ color: "tomato" }} />
          </Right>
        </ListItem>
      );
    }
    return (
      <ListItem
        button
        onPress={() => {
          SocialStore.setInstanceId(item.id);
          SocialStore.setDataPosts([]);
          SocialStore.changeInstance(item.id);
          SocialStore.setModalInstance(false);
        }}
      >
        <Body>
          <Text>{item.name}</Text>
        </Body>
        <Right>
          <Icon name="ios-arrow-forward" />
        </Right>
      </ListItem>
    );
  }

  CloseModalInstance() {
    SocialStore.setModalInstance(false);
  }

  render() {
    return (
      <Container>
        <Content>
          <Header style={{ backgroundColor: "#fafafa" }}>
            <Left />
            <Body>
              <Title style={{ color: "black" }}>Plataformas</Title>
            </Body>
            <Right>
              <Button transparent onPress={this.CloseModalInstance.bind(this)}>
                <Icon name="arrow-down" style={{ color: "#000" }} />
              </Button>
            </Right>
          </Header>
          <View style={{ flex: 1 }}>
            <List>
              <FlatList
                data={Auth.instances}
                renderItem={({ item }) => this.RenderInstances(item)}
                keyExtractor={(item, index) => index.toString()}
              />
            </List>
          </View>
        </Content>
      </Container>
    );
  }
}
