import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon,
  Tabs,
  Tab,
  List,
  ListItem
} from "native-base";
import { Ionicons, SimpleLineIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { ProfileStore } from "../../../../../../../../Store/ProfileStore";

@observer
export default class Pessoal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      name,
      birth_date,
      gender,
      address,
      address2,
      zip_code
    } = this.props.data;

    let date = null;
    let sexo = null;

    if (birth_date != null) {
      let day = birth_date.substring(8, 10);
      let month = birth_date.substring(5, 7);
      let year = birth_date.substring(0, 4);

      date = `${day}/${month}/${year}`;
    }

    if (gender === "M") {
      sexo = "Masculino";
    }

    if (gender === "F") {
      sexo = "Feminino";
    }

    return (
      <List>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Nome</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: name ? "black" : "gray" }}>
              {name || "Nome"}
            </Text>
          </Body>
          <Right />
        </ListItem>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Data Nasc.</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: birth_date ? "black" : "gray" }}>
              {date || "Data de Nascimento"}
            </Text>
          </Body>
          <Right />
        </ListItem>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Sexo</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: gender ? "black" : "gray" }}>
              {sexo || "Sexo"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        {/* <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Telefone</Text>
            </View>
          </Left>
          <Body>
            <Text>(15) 99693-6203 </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Formas de Contato</Text>
            </View>
          </Left>
          <Body>
            <Text>Telefone ou email (teste@teste.com)</Text>
          </Body>
          <Right />
        </ListItem> */}

        {/* <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Endereço</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: address ? "black" : "gray" }}>
              {address || "Endereço"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Complemento</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: address2 ? "black" : "gray" }}>
              {address2 || "Complemento"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>CEP</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: zip_code ? "black" : "gray" }}>
              {zip_code || "CEP"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Cidade</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: ProfileStore.city ? "black" : "gray" }}>
              {ProfileStore.city || "Cidade"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Estado</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: ProfileStore.state ? "black" : "gray" }}>
              {ProfileStore.state || "Estado"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>País</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: ProfileStore.country ? "black" : "gray" }}>
              {ProfileStore.country || "País"}
            </Text>
          </Body>
          <Right />
        </ListItem> */}
      </List>
    );
  }
}

const styles = StyleSheet.create({
  largura: {
    width: 95
  }
});
