import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon,
  Tabs,
  Tab,
  List,
  ListItem
} from "native-base";
import { Ionicons, SimpleLineIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";

@observer
export default class SocialData extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      biography,
      interests,
      activities,
      music,
      books,
      cinema,
      television
    } = this.props.data;
    return (
      <List>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Biografia</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: biography ? "black" : "gray" }}>
              {biography || "Biografia"}
            </Text>
          </Body>
          <Right />
        </ListItem>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Interesses</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: interests ? "black" : "gray" }}>
              {interests || "Interesses"}
            </Text>
          </Body>
          <Right />
        </ListItem>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Atividades</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: activities ? "black" : "gray" }}>
              {activities || "Atividades"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Música</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: music ? "black" : "gray" }}>
              {music || "Música"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Livros</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: books ? "black" : "gray" }}>
              {books || "Livros"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Filmes</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: cinema ? "black" : "gray" }}>
              {cinema || "Filmes"}
            </Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Televisão</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: television ? "black" : "gray" }}>
              {television || "Televisão"}
            </Text>
          </Body>
          <Right />
        </ListItem>
      </List>
    );
  }
}

const styles = StyleSheet.create({
  largura: {
    width: 95
  }
});
