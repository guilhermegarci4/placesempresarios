import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon,
  Tabs,
  Tab,
  List,
  ListItem
} from "native-base";
import { Ionicons, SimpleLineIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";

@observer
export default class Professional extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <List>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Idiomas</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: "gray" }}>Não Disponível</Text>
          </Body>
          <Right />
        </ListItem>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Educação</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: "gray" }}>Não Disponível</Text>
          </Body>
          <Right />
        </ListItem>
        <ListItem avatar>
          <Left>
            <View style={styles.largura}>
              <Text>Exp. Profissional</Text>
            </View>
          </Left>
          <Body>
            <Text style={{ color: "gray" }}>Não Disponível</Text>
          </Body>
          <Right />
        </ListItem>
      </List>
    );
  }
}

const styles = StyleSheet.create({
  largura: {
    width: 95
  }
});
