import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  AsyncStorage,
  TouchableOpacity,
  Modal
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon,
  Tabs,
  Tab,
  List,
  ListItem,
  Thumbnail
} from "native-base";
import { Constants } from "expo";
import { MaterialIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../Store/SocialStore";
import { Auth } from "../../../../../../../Store/Auth";
import { ProfileStore } from "../../../../../../../Store/ProfileStore";
import Loading from "../../../../../../components/loading";
import Instances from "./Instances";
import Pessoal from "./components/Pessoal";
import SocialData from "./components/SocialData";
import Professional from "./components/Professional";
import EditProfile from "./EditProfile";

@observer
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false };
    this.OpenDrawer = this.OpenDrawer.bind(this);
    this.OpenModalInstance = this.OpenModalInstance.bind(this);
    this.OpenModalEdit = this.OpenModalEdit.bind(this);
  }

  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  OpenModalInstance() {
    SocialStore.setModalInstance(true);
  }

  OpenModalEdit() {
    ProfileStore.setModalEdit(true);
  }

  render() {
    return (
      <Container>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Loading visible={this.state.loading} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={this.OpenDrawer}>
              <MaterialIcons name="dehaze" size={25} color="#404040" />
            </Button>
          </Left>
          <Body
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginLeft: Platform.OS === "ios" ? 0 : "21%"
            }}
          >
            <Text
              style={{
                marginRight: 4,
                fontWeight: "bold",
                fontSize: 16,
                color: "#404040"
              }}
            >
              Perfil
            </Text>
          </Body>
          <Right />
        </Header>
        <View style={{ flex: 1, backgroundColor: "#fff" }}>
          <Content>
            <View style={styles.firstSection}>
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  marginLeft: 35
                }}
              >
                <View style={styles.imageProfile}>
                  <Thumbnail
                    large
                    source={{
                      uri:
                        Auth.ProfilePhoto ||
                        "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
                    }}
                  />
                </View>
                <Text>{Auth.name || "Insira seu nome"}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={{
                      justifyContent: "center",
                      alignItems: "center",
                      marginRight: 15
                    }}
                  >
                    <Text style={styles.number}>{ProfileStore.myPosts}</Text>
                    <Text style={styles.subtitle}>posts</Text>
                  </View>
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text style={styles.number}>{Auth.instances.length}</Text>
                    <Text style={styles.subtitle}>plataformas</Text>
                  </View>
                </View>
                <View style={{ alignSelf: "center", flexDirection: "row" }}>
                  <Button
                    bordered
                    style={styles.btnEdit}
                    onPress={this.OpenModalEdit}
                  >
                    <Text style={{ color: "black", fontSize: 14 }}>
                      Editar Perfil
                    </Text>
                  </Button>
                  {/* <Button bordered style={styles.btnEdit}>
                    <SimpleLineIcons name="settings" size={15} color="black" />
                  </Button> */}
                </View>
              </View>
            </View>
            <Tabs tabBarUnderlineStyle={{ backgroundColor: "transparent" }}>
              <Tab
                heading="Pessoal"
                textStyle={{ color: "gray" }}
                activeTextStyle={{ color: "#000" }}
                style={{ padding: 10 }}
                activeTabStyle={{ backgroundColor: "#fff" }}
                tabStyle={{ backgroundColor: "#fff" }}
              >
                <Pessoal data={ProfileStore.userProfile} />
              </Tab>
              <Tab
                heading="Social"
                textStyle={{ color: "gray" }}
                activeTextStyle={{ color: "#000" }}
                activeTabStyle={{ backgroundColor: "#fff" }}
                tabStyle={{ backgroundColor: "#fff" }}
                style={{ padding: 10 }}
              >
                <SocialData data={ProfileStore.userProfile} />
              </Tab>
              <Tab
                heading="Profissional"
                textStyle={{ color: "gray" }}
                activeTextStyle={{ color: "#000" }}
                style={{ padding: 10 }}
                activeTabStyle={{ backgroundColor: "#fff" }}
                tabStyle={{ backgroundColor: "#fff" }}
              >
                <Professional data={ProfileStore.userProfile} />
              </Tab>
            </Tabs>
            <Modal animationType="slide" visible={ProfileStore.modalEdit}>
              <EditProfile data={ProfileStore.userProfile} />
            </Modal>
            <Modal animationType="slide" visible={SocialStore.modalInstance}>
              <Instances />
            </Modal>
          </Content>
        </View>
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#fafafa",
    height: Constants.statusBarHeight
  }
});

const styles = StyleSheet.create({
  firstSection: {
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: "#a6a6a6"
  },
  imageProfile: {
    height: 80,
    width: 80,
    borderRadius: 40,
    margin: 10
  },
  number: {
    fontWeight: "bold"
  },
  subtitle: {
    fontSize: 12,
    color: "gray"
  },
  btnEdit: {
    borderColor: "#cccccc",
    marginTop: 5,
    borderRadius: 5,
    marginRight: 3,
    marginLeft: 2,
    padding: 10,
    height: 30
  }
});
