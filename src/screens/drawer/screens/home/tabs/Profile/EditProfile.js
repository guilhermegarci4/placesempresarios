import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  KeyboardAvoidingView,
  StyleSheet,
  TextInput,
  Alert,
  Keyboard,
  Modal,
  TouchableOpacity,
  Platform
} from "react-native";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon,
  List,
  ListItem,
  Button,
  Input,
  Thumbnail
} from "native-base";
import { observer } from "mobx-react";
import { TextInputMask } from "react-native-masked-text";
import { Auth } from "../../../../../../../Store/Auth";
import { SocialStore } from "../../../../../../../Store/SocialStore";
import { ProfileStore } from "../../../../../../../Store/ProfileStore";
import Loading from "../../../../../../components/loading";
import ActionSheet from "react-native-actionsheet";
import { Constants, ImagePicker, Permissions, ImageManipulator } from "expo";
import DateTimePicker from "react-native-modal-datetime-picker";
import { Dropdown } from "react-native-material-dropdown";

@observer
export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.CloseModalEdit = this.CloseModalEdit.bind(this);
    this.UpdateProfile = this.UpdateProfile.bind(this);
    this._handleGender = this._handleGender.bind(this);

    const standarPhoto =
      "https://mario.aminoapps.com/static/img/user-icon-placeholder.png";
    const {
      name,
      gender,
      address,
      address2,
      zip_code,
      biografy,
      interests,
      activities,
      music,
      books,
      cinema,
      television,
      birth_date
    } = this.props.data;

    let date = null;

    if (birth_date) {
      let day = birth_date.substring(8, 10);
      let month = birth_date.substring(5, 7);
      let year = birth_date.substring(0, 4);

      date = `${day}/${month}/${year}`;
    }

    this.state = {
      editPhoto: Auth.ProfilePhoto || standarPhoto,
      nome: name === null ? "" : name,
      dataNasc: date === null ? "Data de Nasc." : date,
      sexo: gender === null ? "" : gender,
      end: address === null ? "" : address,
      complemento: address2 === null ? "" : address2,
      cep: zip_code === null ? "" : zip_code.toString(),
      cidade: ProfileStore.city == "" ? "" : ProfileStore.city,
      estado: ProfileStore.state == "" ? "" : ProfileStore.state,
      pais: ProfileStore.country == "" ? "" : ProfileStore.country,
      biografia: biografy == null ? "" : biografy,
      interesses: interests == null ? "" : interests,
      atividades: activities == null ? "" : activities,
      musica: music == null ? "" : music,
      livros: books == null ? "" : books,
      filmes: cinema == null ? "" : cinema,
      televisao: television == null ? "" : television,
      modalGender: false,
      isDateTimePickerVisible: false
    };
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    const createdAt = new Date(Date.parse(date));
    const day = ("0" + createdAt.getDate()).slice(-2);
    const month = ("0" + (createdAt.getMonth() + 1)).slice(-2);
    const tranDate = `${day}/${month}/${createdAt.getFullYear()}`;

    this.setState({ dataNasc: tranDate });
    this._hideDateTimePicker();
  };

  CloseModalEdit() {
    ProfileStore.setBaseImage("");
    ProfileStore.setModalEdit(false);
  }

  pickFromGallery = async () => {
    const permissions = Permissions.CAMERA_ROLL;
    const { status } = await Permissions.askAsync(permissions);

    if (status === "granted") {
      let image = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: "Images",
        allowsEditing: true,
        aspect: [4, 3],
        base64: true
        //quality: 0.2
      }).catch(error => console.log(permissions, { error }));

      if (!image.cancelled) {
        this.setState({ editPhoto: image.uri });

        ImageManipulator.manipulate(image.uri, [], {
          base64: true,
          compress: 0
        }).then(img => {
          ProfileStore.setBaseImage(img.base64);
        });
      }
    }
  };

  pickFromCamera = async () => {
    const permissions = Permissions.CAMERA;
    const { status } = await Permissions.askAsync(permissions);

    if (status === "granted") {
      let image = await ImagePicker.launchCameraAsync({
        mediaTypes: "Images",
        allowsEditing: true,
        aspect: [4, 3],
        base64: true
      }).catch(error => console.log(permissions, { error }));

      if (!image.cancelled) {
        this.setState({ editPhoto: image.uri });
        ImageManipulator.manipulate(image.uri, [], {
          base64: true,
          compress: 0
        }).then(img => {
          ProfileStore.setBaseImage(img.base64);
        });
      }
    }
  };

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  selectActionSheetAction(index) {
    if (index == "0") this.pickFromGallery();

    if (index == "1") this.pickFromCamera();
  }

  UpdateProfile() {
    const {
      nome,
      end,
      complemento,
      pais,
      estado,
      cidade,
      sexo,
      biografia,
      interesses,
      atividades,
      musica,
      livros,
      filmes,
      televisao,
      dataNasc,
      cep
    } = this.state;

    let day = dataNasc.substring(0, 2);
    let month = dataNasc.substring(3, 5);
    let year = dataNasc.substring(6, 10);
    let date = `${year}-${month}-${day}`;
    let cepclean = cep.replace("-", "");

    this.setState({ loading: true });
    ProfileStore.updateProfile(
      nome,
      date,
      end,
      complemento,
      cepclean,
      pais,
      estado,
      cidade,
      sexo,
      biografia,
      interesses,
      atividades,
      musica,
      livros,
      filmes,
      televisao
    ).then((res, erro) => {
      Keyboard.dismiss();
      this.setState({ loading: false });
      if (!res) {
        return setTimeout(() => {
          Alert.alert("Erro", erro, [
            { text: "OK", onPress: this.CloseModalEdit }
          ]);
        }, 300);
      }

      setTimeout(() => {
        Alert.alert("Sucesso", "Alteração concluída!", [
          { text: "OK", onPress: this.CloseModalEdit }
        ]);
      }, 300);
    });
  }

  _handleGender(value) {
    if (value === "Masculino") {
      this.setState({ sexo: "M" });
    } else {
      this.setState({ sexo: "F" });
    }
  }

  render() {
    let sexo = "";

    if (this.state.sexo === "M") {
      sexo = "Masculino";
    }

    if (this.state.sexo === "F") {
      sexo = "Feminino";
    }

    let data = [
      {
        value: "Feminino"
      },
      {
        value: "Masculino"
      }
    ];

    return (
      <Container>
        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          title={"De onde deseja escolher sua foto?"}
          options={["Galeria", "Câmera", "Cancelar"]}
          cancelButtonIndex={2}
          onPress={index => {
            this.selectActionSheetAction(index);
          }}
        />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={this.CloseModalEdit}>
              <Text style={{ width: 100 }}>Cancelar</Text>
            </Button>
          </Left>
          <Body
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginLeft: Platform.OS === "ios" ? 0 : "21%"
            }}
          >
            <Title style={{ color: "#000" }}>Editar</Title>
          </Body>
          <Right>
            <Button transparent onPress={this.UpdateProfile}>
              <Text>Salvar</Text>
            </Button>
          </Right>
        </Header>
        <Loading visible={ProfileStore.loading} />
        <View style={{ flex: 1 }}>
          <Content>
            <KeyboardAvoidingView behavior="padding">
              <View style={{ backgroundColor: "#eeeeee" }}>
                <View
                  style={{
                    alignSelf: "center",
                    margin: 10
                  }}
                >
                  <Thumbnail
                    large
                    source={{
                      uri:
                        this.state.editPhoto ||
                        "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
                    }}
                  />
                </View>
                <TouchableOpacity onPress={this.showActionSheet}>
                  <Text
                    style={{
                      alignSelf: "center",
                      marginBottom: 5
                    }}
                  >
                    Alterar Foto
                  </Text>
                </TouchableOpacity>
              </View>
              <List>
                <ListItem itemDivider>
                  <Body>
                    <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                      Pessoal
                    </Text>
                  </Body>
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Nome</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Nome"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      value={this.state.nome}
                      onChangeText={txt => this.setState({ nome: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Data Nasc.</Text>
                    </View>
                  </Left>
                  <Body>
                    <TouchableOpacity onPress={this._showDateTimePicker}>
                      <Text>{this.state.dataNasc}</Text>
                    </TouchableOpacity>
                    <DateTimePicker
                      isVisible={this.state.isDateTimePickerVisible}
                      onConfirm={this._handleDatePicked}
                      onCancel={this._hideDateTimePicker}
                      confirmTextIOS="Confirmar"
                      cancelTextIOS="Cancelar"
                      titleIOS="Selecione a Data"
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Sexo</Text>
                    </View>
                  </Left>
                  <Body>
                    <Dropdown
                      label="Selecione o Sexo"
                      data={data}
                      fontSize={15}
                      onChangeText={this._handleGender}
                      value={sexo}
                    />
                  </Body>
                  <Right />
                </ListItem>

                {/* <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Endereço</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Endereço"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      value={this.state.end}
                      multiline={true}
                      onChangeText={txt => this.setState({ end: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem> */}

                {/* <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Complemento</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Complemento"
                      multiline={true}
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      value={this.state.complemento}
                      onChangeText={txt => this.setState({ complemento: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem> */}

                {/* <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>CEP</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInputMask
                      type={"zip-code"}
                      placeholder="CEP"
                      underlineColorAndroid="transparent"
                      onChangeText={txt => this.setState({ cep: txt })}
                      value={this.state.cep}
                      clearButtonMode="always"
                    />
                  </Body>
                  <Right />
                </ListItem> */}

                {/* <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Cidade</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Cidade"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      value={this.state.cidade}
                      onChangeText={txt => this.setState({ cidade: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem> */}

                {/* <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Estado</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Estado"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      value={this.state.estado}
                      onChangeText={txt => this.setState({ estado: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem> */}

                {/* <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>País</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="País"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      value={this.state.pais}
                      onChangeText={txt => this.setState({ pais: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem> */}

                <ListItem itemDivider>
                  <Body>
                    <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                      Social
                    </Text>
                  </Body>
                </ListItem>

                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Biografia</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Biografia"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      multiline={true}
                      value={this.state.biografia}
                      onChangeText={txt => this.setState({ biografia: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Interesses</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Interesses"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      multiline={true}
                      value={this.state.interesses}
                      onChangeText={txt => this.setState({ interesses: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Atividades</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Atividades"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      multiline={true}
                      value={this.state.atividades}
                      onChangeText={txt => this.setState({ atividades: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Música</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Música"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      multiline={true}
                      value={this.state.musica}
                      onChangeText={txt => this.setState({ musica: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Livros</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Livros"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      multiline={true}
                      value={this.state.livros}
                      onChangeText={txt => this.setState({ livros: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Filmes</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Filmes"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      multiline={true}
                      value={this.state.filmes}
                      onChangeText={txt => this.setState({ filmes: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
                <ListItem avatar>
                  <Left>
                    <View style={styles.largura}>
                      <Text>Televisão</Text>
                    </View>
                  </Left>
                  <Body>
                    <TextInput
                      placeholder="Televisão"
                      placeholderTextColor="#cccccc"
                      underlineColorAndroid="transparent"
                      multiline={true}
                      value={this.state.televisao}
                      onChangeText={txt => this.setState({ televisao: txt })}
                      style={{
                        fontSize: 15
                      }}
                    />
                  </Body>
                  <Right />
                </ListItem>
              </List>
            </KeyboardAvoidingView>
            <Modal animationType="slide" visible={this.state.modalGender}>
              <Header style={{ backgroundColor: "#fafafa" }}>
                <Left />
                <Body>
                  <Title>Sexo</Title>
                </Body>
                <Right>
                  <Button transparent onPress={this.ToggleModalGender}>
                    <Icon name="arrow-down" style={{ color: "black" }} />
                  </Button>
                </Right>
              </Header>
              <List>
                <ListItem button onPress={this.selectMale}>
                  <Text>Masculino</Text>
                </ListItem>
                <ListItem button onPress={this.selectFemale}>
                  <Text>Feminino</Text>
                </ListItem>
              </List>
            </Modal>
          </Content>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  largura: {
    width: 95
  }
});
