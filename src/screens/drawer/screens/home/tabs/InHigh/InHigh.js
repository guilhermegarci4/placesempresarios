import React, { Component, PureComponent } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  Modal,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon
} from "native-base";
import { Constants } from "expo";
import { SimpleLineIcons, Ionicons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../Store/SocialStore";
import ItemView from "./components/ItemView";
import MyFooter from "./components/MyFooter";
import Loading from "../../../../../../components/loading";
import Posts from "./components/Posts";

import MyComponent from "./components/Gifts";
import IconBadge from "react-native-icon-badge";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

const url_assets =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/publication_assets/";

@observer
export default class InHigh extends Component {
  constructor(props) {
    super(props);
    this.state = { refreshing: false };
    window.globalNavigator = this.props.navigation;
    this.OpenDrawer = this.OpenDrawer.bind(this);
  }

  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => {
      SocialStore.getPosts().then(() => {
        this.setState({ refreshing: false });
      });
    });
  };

  initialRender(info) {
    if (info) {
      return (
        <Container>
          <View style={Platform.OS === "ios" ? "" : types.statusBar} />
          <Loading visible={SocialStore.loading} />
          <Header style={{ backgroundColor: "#fafafa" }}>
            <Left>
              <Button transparent onPress={this.OpenDrawer}>
                <SimpleLineIcons name="menu" size={20} color="black" />
              </Button>
            </Left>
            <Body
              style={{
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                marginLeft: Platform.OS === "ios" ? 0 : 50
              }}
            >
              <Image
                source={require("../../../../../../../assets/img/logonew.png")}
                style={{ width: 60, height: 35 }}
                resizeMode="contain"
              />
            </Body>
            <Right />
          </Header>
          <FlatList
            data={SocialStore.inHighPosts}
            renderItem={({ item }) => <Posts data={item} />}
            keyExtractor={(item, index) => index.toString()}
            style={{ flex: 1 }}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
          />
        </Container>
      );
    } else {
      return <Loading visible={true} />;
    }
  }

  render() {
    return this.initialRender(SocialStore.init);
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#fafafa",
    height: Constants.statusBarHeight
  }
});
