import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";

import InHigh from "./InHigh";
import Comments from "./Comments";

const InHighNavigator = createStackNavigator(
  {
    InHigh: {
      screen: InHigh
    },
    Comments: {
      screen: Comments
    }
  },
  {
    initialRouteName: "InHigh",
    navigationOptions: {
      header: null
    }
  }
);

export default InHighNavigator;
