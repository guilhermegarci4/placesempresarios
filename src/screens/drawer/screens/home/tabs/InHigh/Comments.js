import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  KeyboardAvoidingView,
  Dimensions,
  Alert,
  Keyboard
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon,
  Item,
  Input
} from "native-base";
import { Constants } from "expo";
import { Ionicons } from "@expo/vector-icons";
import { SimpleLineIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../Store/SocialStore";
import Loading from "../../../../../../components/loading";
const window = Dimensions.get("window");

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

const url_assets =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/publication_assets/";

@observer
export default class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentWillMount() {
    await SocialStore.getComments(
      this.props.navigation.state.params.pubId,
      data => {
        if (data == "") {
          setTimeout(() => {
            Alert.alert("Informação", "Não há comentários nessa publicação.", [
              { text: "Ok", onPress: () => console.log("fechou") }
            ]);
          }, 300);
        }
      }
    );
  }
  flatRender(item) {
    let avatar = "";
    let gift = "Gift";
    let name = item.name || item.username;

    if (item.gifts.length > 1) {
      gift = "Gifts";
    }

    if (item.thumb_url == null) {
      avatar =
        "https://mario.aminoapps.com/static/img/user-icon-placeholder.png";
    } else {
      avatar = `${url}${item.thumb_url}`;
    }

    return (
      <View>
        <View
          style={{
            height: 60,
            backgroundColor: "white",
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <Image
            style={{
              width: 36,
              height: 36,
              margin: 12,
              borderRadius: 18,
              borderWidth: StyleSheet.hairlineWidth,
              borderColor: "lightgray"
            }}
            source={{ uri: avatar }}
          />
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              paddingRight: 20
            }}
          >
            <Text>
              <Text style={{ fontSize: 12, fontWeight: "bold" }}>{name} </Text>
              <Text style={{ marginLeft: 5, fontSize: 12 }}>
                {item.message}
              </Text>
            </Text>
          </View>
        </View>
      </View>
    );
  }

  PostComment() {
    SocialStore.setInitComments(true);
    if (SocialStore.textCommentPost != "") {
      Keyboard.dismiss();
      SocialStore.postComment(this.props.navigation.state.params.pubId);
    } else {
      SocialStore.setInitComments(false);
      setTimeout(() => {
        Alert.alert("Atenção", "Escreva algum comentário", [
          { text: "Ok", onPress: () => console.log("fechou") }
        ]);
      }, 300);
    }
  }

  render() {
    return (
      <Container>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: "black" }} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color: "gray" }}>Comentários</Title>
          </Body>
          <Right />
        </Header>
        <Loading visible={SocialStore.initComments} />
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
          <View style={{ flex: 1, backgroundColor: "white" }}>
            <Content>
              <FlatList
                data={SocialStore.dataComments}
                renderItem={({ item }) => this.flatRender(item)}
                keyExtractor={(item, index) => index.toString()}
                style={{ flex: 1 }}
              />
            </Content>
          </View>
          <View
            style={{
              backgroundColor: "white",
              flexDirection: "row",
              alignItems: "center",
              borderTopWidth: 0.5,
              borderColor: "gray"
            }}
          >
            <Image
              style={{
                width: 34,
                height: 34,
                margin: 12,
                borderRadius: 17,
                borderWidth: StyleSheet.hairlineWidth,
                borderColor: "lightgray"
              }}
              source={{
                uri:
                  "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
              }}
            />
            <Item regular style={styles.iteminput}>
              <Input
                placeholder="Adicionar Comentário"
                style={styles.txtinput}
                placeholderTextColor="gray"
                value={SocialStore.textCommentPost}
                onChangeText={txt => SocialStore.setTextCommentPost(txt)}
              />
            </Item>
            <Button
              transparent
              style={{ alignSelf: "center", marginLeft: 10 }}
              onPress={this.PostComment.bind(this)}
            >
              <Text style={{ color: "tomato", alignSelf: "center" }}>
                Postar
              </Text>
            </Button>
          </View>
        </KeyboardAvoidingView>
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#fafafa",
    height: Constants.statusBarHeight
  }
});

const styles = StyleSheet.create({
  iteminput: {
    height: window.width / 12,
    width: window.width - 150,
    margin: 5,
    borderRadius: 5
  },
  txtinput: {
    fontSize: 12,
    height: window.width / 12,
    width: window.width - 150,
    borderColor: "transparent"
  }
});
