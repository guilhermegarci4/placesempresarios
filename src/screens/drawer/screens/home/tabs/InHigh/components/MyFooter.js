import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Button,
  TouchableHighlight
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { Auth } from "../../../../../../../../Store/Auth";

@observer
export default class MyFooter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fcmessage: this.props.cutmessage,
      changecolor: "gray"
    };
    this.ShowMore = this.ShowMore.bind(this);
  }

  ShowMore = message => () => {
    this.setState({
      fcmessage: message,
      changecolor: "#fff"
    });
  };

  render() {
    const { message, press, name, comment, date } = this.props;
    if (this.props.type === "image") {
      if (message.length > 190) {
        return (
          <View>
            <View
              style={{
                marginTop: 10,
                flexDirection: "row",
                flex: 1
              }}
            >
              <Text>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                  {name}{" "}
                </Text>
                <Text style={{ marginLeft: 5, fontSize: 12 }}>
                  {this.state.fcmessage}
                  <Text
                    style={{
                      color: this.state.changecolor,
                      fontSize: 15,
                      marginBottom: -4
                    }}
                    onPress={this.ShowMore(message)}
                  >
                    ...Mais
                  </Text>
                </Text>
              </Text>
            </View>
            <TouchableOpacity onPress={press}>
              <View style={{ marginTop: 5 }}>
                <Text style={{ color: "gray" }}>{comment}</Text>
              </View>
            </TouchableOpacity>
            <View style={{ height: 30 }} />
          </View>
        );
      } else {
        return (
          <View>
            <View
              style={{
                marginTop: 10,
                flexDirection: "row",
                flex: 1
              }}
            >
              <Text>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>
                  {name}{" "}
                </Text>
                <Text style={{ marginLeft: 5, fontSize: 12 }}>{message}</Text>
              </Text>
            </View>
            <TouchableOpacity onPress={press}>
              <View style={{ marginTop: 5 }}>
                <Text style={{ color: "gray" }}>{comment}</Text>
              </View>
            </TouchableOpacity>
            <View style={{ height: 30 }} />
          </View>
        );
      }
    } else {
      return (
        <View>
          <TouchableOpacity onPress={press}>
            <View style={{ marginTop: 5 }}>
              <Text style={{ color: "gray" }}>{comment}</Text>
            </View>
          </TouchableOpacity>
          <View style={{ height: 30 }} />
        </View>
      );
    }
  }
}

//   if (this.props.message.length > 190) {
//     return (
//       <View style={{ padding: 20, backgroundColor: "#fff" }}>
//         <Text>
//           {this.state.message}
//           <TouchableHighlight
//             onPress={() => {
//               this.setState({
//                 message: this.props.message,
//                 changecolor: "#fff"
//               });
//             }}
//           >
//             <Text
//               style={{
//                 color: this.state.changecolor,
//                 fontSize: 15,
//                 marginBottom: -4
//               }}
//             >
//               ...Mais
//             </Text>
//           </TouchableHighlight>
//         </Text>
//       </View>
//     );
//   }

//   return (
//     <View style={{ padding: 20, backgroundColor: "#fff" }}>
//       <Text>{this.props.message}</Text>
//     </View>
//   );
// }
