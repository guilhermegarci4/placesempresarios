import React, { Component, PureComponent } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  Modal,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  ScrollView
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon
} from "native-base";
import { Constants } from "expo";
import { SimpleLineIcons, Ionicons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../../Store/SocialStore";
import ItemView from "./ItemView";
import MyFooter from "./MyFooter";
import MyComponent from "./Gifts";
import DoubleClick from "react-native-double-click";
import { Auth } from "../../../../../../../../Store/Auth";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

const url_assets =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/publication_assets/";

@observer
export default class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = { quantity: this.props.data.gifts.length, colorGift: "black" };
    this.SendGift = this.SendGift.bind(this);
  }

  SendGift = id => () => {
    SocialStore.postGift(id, res => {
      //console.log("TEXTO DO GIFT", res);
      if (res.success && res.id != 0) {
        let growUp = this.state;
        growUp.quantity = this.state.quantity + 1;
        growUp.colorGift = "tomato";
        this.setState(growUp);
      }
      if (res.success && res.id === 0) {
        let growUp = this.state;
        growUp.quantity = this.state.quantity - 1;
        growUp.colorGift = "black";
        this.setState(growUp);
      }
    });
  };

  componentDidMount() {
    this.props.data.gifts.find(obj => {
      if (obj.user_id === Auth.id) {
        this.setState({ colorGift: "tomato" });
      }
    });
  }

  render() {
    const {
      created_at,
      gifts,
      thumb_url,
      message,
      name,
      username,
      pub_id,
      asset_url,
      asset_id,
      preview_original_asset_url
    } = this.props.data;

    let isimage = "image";
    let avatar = "";
    let gift = "Gift";
    let uname = name || username;
    let comments = "Visualizar comentários";
    let colorg = "black";

    const createdAt = new Date(Date.parse(created_at));
    const day = ("0" + createdAt.getDate()).slice(-2);
    const month = ("0" + (createdAt.getMonth() + 1)).slice(-2);
    const tranDate = `${day}/${month}/${createdAt.getFullYear()}`;

    if (gifts.length > 1) {
      gift = "Gifts";
    }

    if (thumb_url == null) {
      avatar =
        "https://mario.aminoapps.com/static/img/user-icon-placeholder.png";
    } else {
      avatar = `${url}${thumb_url}`;
    }

    let cut = message.substr(0, 190);

    return (
      <View>
        <View
          style={{
            height: 60,
            backgroundColor: "white",
            flexDirection: "row"
          }}
        >
          <Image
            style={{
              width: 36,
              height: 36,
              margin: 12,
              borderRadius: 18,
              borderWidth: StyleSheet.hairlineWidth,
              borderColor: "lightgray"
            }}
            source={{ uri: avatar }}
          />
          <View
            style={{
              paddingTop: 15,
              flex: 1
            }}
          >
            <Text
              style={{
                fontWeight: "bold"
              }}
            >
              {name || username}
            </Text>
            <Text
              style={{
                fontSize: 9,
                color: "#cccccc",
                marginTop: 3
              }}
            >
              Postado em {tranDate}
            </Text>
          </View>
          {/* <Ionicons
            name="ios-more"
            size={30}
            color="black"
            style={{ lineHeight: 60, marginRight: 15 }}
          /> */}
        </View>

        <ItemView
          type={isimage}
          hasimage={asset_url}
          source={`${url_assets}${asset_id}/${preview_original_asset_url}`}
          message={message}
          cutmessage={cut}
        />

        <View
          style={{
            backgroundColor: "#fff"
          }}
        >
          <View
            style={{
              height: 54,
              backgroundColor: "white",
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity onPress={this.SendGift(pub_id)}>
              <SimpleLineIcons
                name="present"
                size={25}
                color={this.state.colorGift}
                style={{ marginTop: 12, marginLeft: 15 }}
              />
            </TouchableOpacity>
            {/* <SimpleLineIcons
              name="bubble"
              size={25}
              color="black"
              style={{ marginTop: 12, marginLeft: 15 }}
            /> */}
            <View style={{ flex: 1 }} />
            {/* <Ionicons
              name="ios-bookmark-outline"
              size={34}
              color="black"
              style={{ marginTop: 12, marginRight: 15 }}
            /> */}
          </View>
          <Text style={{ paddingLeft: 15, fontSize: 13, fontWeight: "bold" }}>
            {this.state.quantity} {gift}
          </Text>
        </View>
        <View
          style={{
            paddingLeft: 15,
            backgroundColor: "#fff",
            paddingRight: 15
          }}
        >
          <MyFooter
            type={isimage}
            message={message}
            cutmessage={cut}
            name={uname}
            press={() =>
              window.globalNavigator.navigate("Comments", {
                pubId: pub_id
              })
            }
            comment={comments}
            date={tranDate}
          />
        </View>
      </View>
    );
  }
}
