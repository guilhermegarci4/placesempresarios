import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Button,
  TouchableHighlight
} from "react-native";
import { Ionicons, SimpleLineIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../../Store/SocialStore";

@observer
export default class Gifts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: this.props.number,
      ident: this.props.id
    };
    this.SendGift = this.SendGift.bind(this);
  }

  SendGift = id => () => {
    SocialStore.postGift(id, res => {
      console.log("TEXTO DO GIFT", res);
      if (res.success && res.id != 0) {
        let growUp = this.state;
        growUp.quantity = this.state.quantity + 1;
        this.setState(growUp);
      }
      if (res.success && res.id === 0) {
        let growUp = this.state;
        growUp.quantity = this.state.quantity - 1;
        this.setState(growUp);
      }
    });
  };

  render() {
    const { txt, number, id } = this.props;
    return (
      <View
        style={{
          backgroundColor: "#fff"
        }}
      >
        <View
          style={{
            height: 54,
            backgroundColor: "white",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity onPress={this.SendGift(id)}>
            <SimpleLineIcons
              name="present"
              size={25}
              color="black"
              style={{ marginTop: 12, marginLeft: 15 }}
            />
          </TouchableOpacity>
          <SimpleLineIcons
            name="bubble"
            size={25}
            color="black"
            style={{ marginTop: 12, marginLeft: 15 }}
          />
          <View style={{ flex: 1 }} />
          <Ionicons
            name="ios-bookmark-outline"
            size={34}
            color="black"
            style={{ marginTop: 12, marginRight: 15 }}
          />
        </View>
        <Text style={{ paddingLeft: 15, fontSize: 13, fontWeight: "bold" }}>
          {this.state.quantity} {txt}
        </Text>
      </View>
    );
  }
}
