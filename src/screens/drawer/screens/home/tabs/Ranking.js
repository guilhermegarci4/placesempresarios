import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  AsyncStorage
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon
} from "native-base";
import { Constants } from "expo";
//import Video from "../../../../../../../../Library/Caches/typescript/2.9/node_modules/@types/react-native-video";
import { Ionicons, SimpleLineIcons } from "@expo/vector-icons";
import { RankingStore } from "../../../../../../Store/RankingStore";
import Loading from "../../../../../components/loading";
import { observer } from "mobx-react";

@observer
export default class Ranking extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false };
    this.OpenDrawer = this.OpenDrawer.bind(this);
  }

  // componentWillMount() {
  //   RankingStore.getRanking(() => {
  //     setTimeout(() => {
  //       this.setState({ loading: false });
  //     }, 800);
  //   });
  // }

  RenderRanking(item, index) {
    return (
      <View>
        <View
          style={{
            height: 60,
            backgroundColor: "white",
            flexDirection: "row"
          }}
        >
          <Text style={{ marginLeft: 15, lineHeight: 50, marginTop: 5 }}>
            {index + 1}º
          </Text>
          {/* <Image
            style={{
              width: 36,
              height: 36,
              margin: 12,
              borderRadius: 18,
              borderWidth: StyleSheet.hairlineWidth,
              borderColor: "lightgray"
            }}
            source={{ uri: item.avatarUrl }}
          /> */}
          <Text
            style={{
              fontWeight: "bold",
              height: 60,
              lineHeight: 60,
              flex: 1
            }}
          >
            {" "}
            {item.username}
          </Text>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{ lineHeight: 50, alignSelf: "center", paddingBottom: 5 }}
            >
              {item.gifts}
            </Text>
            <SimpleLineIcons
              name="present"
              size={25}
              color="black"
              style={{ marginTop: 12, marginLeft: 15, marginRight: 15 }}
            />
          </View>
        </View>
      </View>
    );
  }

  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  render() {
    return (
      <Container>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Loading visible={this.state.loading} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={this.OpenDrawer}>
              <SimpleLineIcons name="menu" size={20} color="black" />
            </Button>
          </Left>
          <Body  style={{
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                marginLeft: Platform.OS === "ios" ? 0 : 50
              }}>
            <Image
              source={require("../../../../../../assets/img/logonew.png")}
              style={{ width: 60, height: 35 }}
              resizeMode="contain"
            />
          </Body>
          <Right />
        </Header>
        <Content>
          <View style={{ margin: 20, flexDirection: "row" }}>
            <SimpleLineIcons
              name="trophy"
              size={25}
              color="black"
              style={{ fontWeight: "bold", marginRight: 10 }}
            />
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              Ranking Gifts
            </Text>
          </View>
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            style={{ flex: 1 }}
            data={RankingStore.ranking}
            renderItem={({ item, index }) => this.RenderRanking(item, index)}
          />
        </Content>
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#fafafa",
    height: Constants.statusBarHeight
  }
});
