import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  KeyboardAvoidingView,
  Dimensions,
  Alert,
  Keyboard,
  ScrollView
} from "react-native";
import {
  Header,
  Container,
  Content,
  Left,
  Right,
  Title,
  Body,
  Button,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";
import { Constants } from "expo";
import { Ionicons } from "@expo/vector-icons";
import { SimpleLineIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../Store/SocialStore";
import { RankingStore } from "../../../../../../../Store/RankingStore";
import { Auth } from "../../../../../../../Store/Auth";
import Loading from "../../../../../../components/loading";
import { VisitProfileStore } from "../../../../../../../Store/VisitProfileStore";
import Posts from "./components/Posts";
import Hyperlink from "react-native-hyperlink";
const window = Dimensions.get("window");

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

const url_assets =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/publication_assets/";

@observer
export default class VisitProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberPosts: 0,
      posts: [],
      gifts: 0,
      instanceName: ""
    };
  }

  componentWillMount() {
    VisitProfileStore.getDataProfile(
      this.props.navigation.state.params.idprofile
    );
  }

  componentDidMount() {
    let countposts = 0;
    let postedby = [];
    SocialStore.dataPosts.find(obj => {
      if (obj.profile_id === this.props.navigation.state.params.idprofile) {
        postedby.push(obj);
        countposts += 1;
      }
    });
    postedby.sort(function(a, b) {
      return b.created_at - a.created_at;
    });

    this.setState({ numberPosts: countposts, posts: postedby });

    RankingStore.ranking.find(obj => {
      if (obj.profile_id === this.props.navigation.state.params.idprofile) {
        this.setState({ gifts: obj.gifts });
      }
    });

    Auth.instances.find(obj => {
      if (obj.id === Auth.defaultInstance) {
        this.setState({ instanceName: obj.name });
      }
    });
  }

  initialRender(info) {
    if (info) {
      return (
        <View>
          <View style={styles.firstSection}>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View style={styles.imageProfile}>
                <Thumbnail
                  large
                  source={{
                    uri: this.props.navigation.state.params.thumb
                  }}
                />
              </View>
              {/* <Text>{uname}</Text> */}
            </View>
            <View style={{ flex: 1 }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-around"
                }}
              >
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <Text style={styles.number}>{this.state.numberPosts}</Text>
                  <Text style={styles.subtitle}>posts</Text>
                </View>
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <Text style={styles.number}>{this.state.gifts}</Text>
                  <Text style={styles.subtitle}>Gifts</Text>
                </View>
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <Text style={styles.number}>São Judas</Text>
                  <Text style={styles.subtitle}>Plataforma</Text>
                </View>
              </View>
              {/* <View style={{ alignSelf: "center", flexDirection: "row" }}>
                <Button bordered style={styles.btnEdit}>
                  <Text
                    style={{
                      color: "black",
                      fontSize: 14,
                      fontWeight: "bold",
                      paddingLeft: 45,
                      paddingRight: 45
                    }}
                  >
                    Mensagem
                  </Text>
                </Button>
                <Button bordered style={styles.btnEdit}>
                  <SimpleLineIcons
                    name="user-following"
                    size={15}
                    color="black"
                  />
                </Button>
              </View> */}
            </View>
          </View>
          <View style={styles.secondSection}>
            <View>
              <Text style={{ marginLeft: 10, fontWeight: "bold" }}>
                {this.props.navigation.state.params.uname}
              </Text>
            </View>
            <View>
              <Text style={styles.textInfo}>
                Nasceu em{" "}
                {VisitProfileStore.birthDate || "Não informado pelo usuário"}
              </Text>
              <Text style={styles.textInfo}>
                Mora em {VisitProfileStore.city || "Não informado pelo usuário"}
              </Text>
              <Hyperlink
                linkDefault={true}
                linkStyle={{ color: "#2980b9", fontSize: 13 }}
              >
                <Text
                  style={{
                    marginLeft: 10,
                    marginTop: 3,
                    paddingBottom: 30,
                    color: "gray"
                  }}
                >
                  {VisitProfileStore.dataProfile.biography || "Biografia"}
                </Text>
              </Hyperlink>
            </View>
          </View>
          <View style={styles.separator}>
            <Text
              style={{ alignSelf: "center", fontSize: 15, fontWeight: "bold" }}
            >
              Publicações de {this.props.navigation.state.params.uname}
            </Text>
          </View>
          <FlatList
            data={this.state.posts}
            renderItem={({ item }) => <Posts data={item} />}
            keyExtractor={(item, index) => index.toString()}
            style={{ flex: 1 }}
          />
        </View>
      );
    } else {
      <Loading visible={true} />;
    }
  }

  render() {
    const { uname, idprofile, thumb } = this.props.navigation.state.params;
    return (
      <Container>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: "black" }} />
            </Button>
          </Left>
          <Body>
            <Title style={{ color: "black" }}>{uname}</Title>
          </Body>
          <Right />
        </Header>
        <ScrollView>{this.initialRender(VisitProfileStore.init)}</ScrollView>
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#fafafa",
    height: Constants.statusBarHeight
  }
});

const styles = StyleSheet.create({
  firstSection: {
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
    // borderBottomWidth: 0.5,
    // borderBottomColor: "#a6a6a6"
  },
  secondSection: {
    backgroundColor: "white"
  },
  imageProfile: {
    height: 80,
    width: 80,
    borderRadius: 40,
    margin: 10
  },
  number: {
    fontWeight: "bold"
  },
  subtitle: {
    fontSize: 12,
    color: "gray"
  },
  btnEdit: {
    borderColor: "#cccccc",
    marginTop: 5,
    borderRadius: 5,
    marginRight: 3,
    marginLeft: 2,
    padding: 10,
    height: 30
  },
  textInfo: {
    fontSize: 10,
    color: "#cccccc",
    marginLeft: 10,
    marginTop: 2
  },
  separator: {
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: "#fff",
    width: window.width,
    borderTopWidth: 0.5,
    borderTopColor: "#a6a6a6",
    borderBottomWidth: 0.5,
    borderBottomColor: "#a6a6a6"
  }
});
