import React, { Component } from "react";
import autobind from "autobind-decorator";
import {
  View,
  Image,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  TouchableOpacity
} from "react-native";
import { Header, Container, Left, Right, Body, Button } from "native-base";
import { Constants } from "expo";
import { MaterialIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../Store/SocialStore";
import Loading from "../../../../../../components/loading";
import Notifications from "./Notifications/index";
import IconBadge from "react-native-icon-badge";
import Posts from "./components/Posts";

import { Auth } from "../../../../../../../Store/Auth";

@observer
export default class Social extends Component {
  constructor(props) {
    super(props);
    this.state = {
      BadgeCount: 5,
      refreshing: false
    };
    window.globalNavigator = this.props.navigation;
  }

  @autobind
  OpenModalNotification() {
    this.props.navigation.navigate("Notifications");
  }

  @autobind
  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => {
      SocialStore.getPosts().then(() => {
        this.setState({ refreshing: false });
      });
    });
  };

  @autobind
  handleGift(id) {
    //console.log("deu gift");
    SocialStore.postGift(id);
  }

  @autobind
  handleDelete(id) {
    SocialStore.deletePublish(id);
  }

  initialRender(info) {
    if (info) {
      return (
        <Container>
          <View style={Platform.OS === "ios" ? "" : types.statusBar} />
          <Loading visible={SocialStore.loading} />
          <Header style={{ backgroundColor: "#fafafa" }}>
            <Left>
              <Button transparent onPress={this.OpenDrawer}>
                <MaterialIcons name="dehaze" size={25} color="#404040" />
              </Button>
            </Left>
            <Body
              style={{
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                marginLeft: Platform.OS === "ios" ? 0 : "21%"
              }}
            >
              <Image
                source={require("../../../../../../../assets/logoLetter.png")}
                style={{
                  width: 120,
                  height: 40
                }}
                resizeMode="contain"
              />
            </Body>
            <Right>
              <View style={{ flexDirection: "row" }}>
                {/* <TouchableOpacity onPress={this.OpenModalNotification}>
                  <IconBadge
                    MainElement={
                      <View
                        style={{
                          backgroundColor: "transparent",
                          width: 30,
                          height: 30,
                          margin: 6
                        }}
                      >
                        <MaterialIcons
                          name="notifications"
                          size={25}
                          color="#404040"
                        />
                      </View>
                    }
                    BadgeElement={
                      <Text style={{ color: "#FFFFFF", fontSize: 10 }}>
                        {SocialStore.badgeNotification}
                      </Text>
                    }
                    Hidden={SocialStore.badgeNotification == 0}
                    //IconBadgeStyle={{ backgroundColor: "orange" }}
                  />
                </TouchableOpacity> */}
              </View>
            </Right>
          </Header>

          <FlatList
            data={SocialStore.dataPosts}
            renderItem={({ item }) => (
              <Posts
                data={item}
                id={Auth.id}
                nav={this.props.navigation}
                likeAction={this.handleGift}
                deletePost={this.handleDelete}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
            style={{ flex: 1 }}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
          />
        </Container>
      );
    } else {
      return <Loading visible={true} />;
    }
  }

  render() {
    return this.initialRender(SocialStore.init);
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#FAFAFA",
    height: Constants.statusBarHeight
  }
});
