import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";

import Social from "./Social";
import Comments from "./Comments";
import Posts from "./components/Posts";
import VisitProfile from "./VisitProfile";
import Notifications from "./Notifications/Notifications";
import ViewPublication from "./Notifications/ViewPublication";
import ListNotification from "./components/ListNotification";

const SocialNavigator = createStackNavigator(
  {
    Social: {
      screen: Social
    },
    Comments: {
      screen: Comments
    },
    Posts: {
      screen: Posts
    },
    VisitProfile: {
      screen: VisitProfile
    },
    Notifications: {
      screen: Notifications
    },
    ViewPublication: {
      screen: ViewPublication
    },
    ListNotification: {
      screen: ListNotification
    }
  },
  {
    initialRouteName: "Social",
    navigationOptions: {
      header: null
    }
  }
);

export default SocialNavigator;
