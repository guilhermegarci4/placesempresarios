import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon,
  List,
  ListItem,
  Button,
  Thumbnail
} from "native-base";
import { withNavigation } from "react-navigation";
import { observer } from "mobx-react";
import { Auth } from "../../../../../../../../Store/Auth";
import { SocialStore } from "../../../../../../../../Store/SocialStore";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

@observer
class ListNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      colorRU: "#fff"
    };
    this._handleViewPub = this._handleViewPub.bind(this);
  }

  _handleViewPub = (id, notificationid, isread) => () => {
    if (!isread) {
      SocialStore.readNotification(notificationid);
      this.setState({ colorRU: "#fff" });
    }

    this.props.navigation.navigate("ViewPublication", {
      idpub: id
    });
  };

  componentDidMount() {
    if (!this.props.data.read) {
      this.setState({ colorRU: "#ffdecc" });
    }
  }

  render() {
    const { data, press } = this.props;
    let { username, thumb_url } = data.origin.author;
    let { message } = data.origin;
    let cutmessage = message;
    let imageprofile = null;

    if (thumb_url != null) {
      imageprofile = `${url}${thumb_url}`;
    }

    if (message.length > 29) {
      cutmessage = `${message.substring(0, 30)}...`;
    }

    if (username === Auth.name) {
      return <View />;
    }

    return (
      <View style={{ flex: 1, backgroundColor: this.state.colorRU }}>
        <ListItem
          thumbnail
          button
          onPress={this._handleViewPub(data.origin_id, data.id, data.read)}
        >
          <Left>
            <Thumbnail
              source={{
                uri:
                  imageprofile ||
                  "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
              }}
            />
          </Left>
          <Body>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                flexWrap: "wrap"
              }}
            >
              <Text style={{ fontWeight: "bold", marginRight: 5 }}>
                {username}
              </Text>
              <Text numberOfLines={2}>fez uma publicação</Text>
            </View>
            <Text note numberOfLines={1} style={{ color: "gray" }}>
              {cutmessage}
            </Text>
          </Body>
          <Right>
            <Icon name="more" />
            {/* <Button transparent>
                <Text>Ver</Text>
              </Button> */}
          </Right>
        </ListItem>
      </View>
    );

    // return (
    //   <View style={{ flex: 1, backgroundColor: "#fff" }}>
    //     <ListItem
    //       thumbnail
    //       button
    //       onPress={this._handleViewPub(data.origin_id, data.id)}
    //     >
    //       <Left>
    //         <Thumbnail
    //           source={{
    //             uri:
    //               imageprofile ||
    //               "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
    //           }}
    //         />
    //       </Left>
    //       <Body>
    //         <View
    //           style={{
    //             flexDirection: "row",
    //             flex: 1,
    //             flexWrap: "wrap"
    //           }}
    //         >
    //           <Text style={{ fontWeight: "bold", marginRight: 5 }}>
    //             {username}
    //           </Text>
    //           <Text numberOfLines={2}>fez uma publicação</Text>
    //         </View>
    //         <Text note numberOfLines={1} style={{ color: "gray" }}>
    //           {cutmessage}
    //         </Text>
    //       </Body>
    //       <Right>
    //         <Icon name="more" />
    //       </Right>
    //     </ListItem>
    //   </View>
    // );
  }
}

export default withNavigation(ListNotification);
