import React, { Component, PureComponent } from "react";
import autobind from "autobind-decorator";
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Alert,
  TouchableHighlight
} from "react-native";
import { SimpleLineIcons, Ionicons, MaterialIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../../Store/SocialStore";
import ItemView from "./ItemView";
import MyFooter from "./MyFooter";
import { Auth } from "../../../../../../../../Store/Auth";
import { withNavigation } from "react-navigation";
import moment from "moment";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

const url_assets =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/publication_assets/";

class Posts extends PureComponent {
  constructor(props) {
    super(props);

    let likebyme = false;
    this.props.data.gifts.find(obj => {
      if (obj.user_id === this.props.id) {
        likebyme = true;
      }
    });

    this.state = {
      quantity: this.props.data.gifts.length,
      colorGift: "black",
      nGift: 0,
      isLiked: likebyme
    };
  }

  @autobind
  SendGift() {
    this.props.likeAction(this.props.data.pub_id);

    if (this.state.isLiked) {
      this.setState({ quantity: this.state.quantity - 1, isLiked: false });
    } else {
      this.setState({ quantity: this.state.quantity + 1, isLiked: true });
    }
  }

  handleVisit = (id, name, avatar) => () => {
    this.props.nav.navigate("VisitProfile", {
      idprofile: id,
      uname: name,
      thumb: avatar
    });
  };

  @autobind
  handleDelete() {
    this.props.deletePost(this.props.data.pub_id);
  }

  @autobind
  confirmDelete() {
    Alert.alert(
      "Atenção",
      "Deseja realmente excluir essa publicação?",
      [
        { text: "Confirmar", onPress: this.handleDelete },
        { text: "Cancelar", style: "cancel" }
      ],
      { cancelable: false }
    );
  }

  @autobind
  photoClick() {
    let state = this.state;
    state.nGift++;
    this.setState(state);

    if (state.nGift == 2) {
      this.SendGift();
    }

    setTimeout(() => {
      let state = this.state;
      state.nGift = 0;
      this.setState(state);
    }, 300);
  }

  render() {
    if (this.props.data === {}) {
      return <View />;
    }
    const {
      created_at,
      gifts,
      thumb_url,
      message,
      name,
      username,
      pub_id,
      asset_url,
      asset_id,
      preview_original_asset_url,
      profile_id,
      user_id
    } = this.props.data;

    let isimage = "image";
    let avatar = "";
    let gift = "Gift";
    let uname = name || username;
    let comments = "Comentários";
    let ispdf = 0;
    let isexcel = 0;
    let cloudicon = true;
    let colorCloudIcon = "transparent";
    let isMine = false;
    let giftColor = "#404040";

    if (this.state.isLiked) {
      giftColor = "blue";
    }

    if (this.props.id === user_id) {
      isMine = true;
    }

    const createdAt = new Date(Date.parse(created_at));
    let tranDate = moment(createdAt).format("DD/MM/YYYY");

    if (asset_url != null) {
      ispdf = asset_url.search(".pdf");
      isexcel = asset_url.search(".xls");
    }

    if (ispdf > 0) {
      //console.log("ENTREI NO ISPDF");
      isimage = "pdf";
      cloudicon = false;
      colorCloudIcon = "black";
    }

    if (isexcel > 0) {
      isimage = "excel";
      cloudicon = false;
      colorCloudIcon = "black";
    }

    if (gifts.length > 1) {
      gift = "Gifts";
    }

    if (thumb_url == null) {
      avatar =
        "https://mario.aminoapps.com/static/img/user-icon-placeholder.png";
    } else {
      avatar = `${url}${thumb_url}`;
    }

    let cut = message.substr(0, 190);

    return (
      <View>
        <View style={styles.viewHeader}>
          <TouchableOpacity
            onPress={this.handleVisit(profile_id, uname, avatar)}
          >
            <Image style={styles.img} source={{ uri: avatar }} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.basicFlex}
            onPress={this.handleVisit(profile_id, uname, avatar)}
          >
            <View style={styles.viewUsername}>
              <Text style={styles.textUsername}>{name || username}</Text>
              <Text style={styles.textPostDate}>Postado em {tranDate}</Text>
            </View>
          </TouchableOpacity>
          {isMine && (
            <TouchableOpacity onPress={this.confirmDelete}>
              <MaterialIcons
                name="delete-forever"
                size={25}
                color="#404040"
                style={styles.iconDelete}
              />
            </TouchableOpacity>
          )}
        </View>

        <TouchableHighlight
          underlayColor={null}
          activeOpacity={1}
          onPress={this.photoClick}
        >
          <ItemView
            type={isimage}
            hasimage={asset_url}
            source={`${url_assets}${asset_id}/${preview_original_asset_url}`}
          />
        </TouchableHighlight>
        <View style={styles.viewInterationWrapper}>
          <View style={styles.viewInterationContainer}>
            <TouchableOpacity onPress={this.SendGift}>
              <SimpleLineIcons
                name="present"
                size={25}
                color={giftColor}
                style={{ marginTop: 12, marginLeft: 15 }}
              />
            </TouchableOpacity>
            {/* <SimpleLineIcons
              name="bubble"
              size={25}
              color="black"
              style={{ marginTop: 12, marginLeft: 15 }}
            /> */}
            <View style={{ flex: 1 }} />
            <TouchableOpacity
              disabled={cloudicon}
              onPress={() => {
                Linking.openURL(
                  `${url_assets}${asset_id}/${preview_original_asset_url}`
                );
              }}
            >
              <SimpleLineIcons
                name="cloud-download"
                size={25}
                color={colorCloudIcon}
                style={{ marginTop: 12, marginRight: 15 }}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.viewGifts}>
            {this.state.quantity} {gift}
          </Text>
        </View>
        <View style={styles.viewFooter}>
          <MyFooter
            type={isimage}
            message={message}
            cutmessage={cut}
            name={uname}
            press={() =>
              this.props.navigation.navigate("Comments", {
                pubId: pub_id
              })
            }
            comment={comments}
            date={tranDate}
          />
        </View>
      </View>
    );
  }
}

export default withNavigation(Posts);

const styles = StyleSheet.create({
  // GENERIC
  basicFlex: {
    flex: 1
  },
  viewHeader: {
    height: 60,
    backgroundColor: "white",
    flexDirection: "row"
  },
  img: {
    width: 36,
    height: 36,
    margin: 12,
    borderRadius: 18,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "lightgray"
  },
  viewUsername: {
    paddingTop: 15,
    flex: 1
  },
  textUsername: {
    fontWeight: "bold"
  },
  textPostDate: {
    fontSize: 9,
    color: "#cccccc",
    marginTop: 3
  },
  iconDelete: {
    lineHeight: 60,
    marginRight: 15
  },
  viewInterationWrapper: {
    backgroundColor: "#fff"
  },
  viewInterationContainer: {
    height: 54,
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  viewGifts: {
    paddingLeft: 15,
    fontSize: 13,
    fontWeight: "bold"
  },
  viewFooter: {
    paddingLeft: 15,
    backgroundColor: "#fff",
    paddingRight: 15
  }
});
