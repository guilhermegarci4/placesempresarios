import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  Image,
  TouchableOpacity
} from "react-native";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

const url_assets =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/publication_assets/";

export default class FeedItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      color: false
    };
    this.handleLike = this.handleLike.bind(this);
  }

  componentDidUpdate(props) {
    console.log("o que mudou", props);
  }

  handleLike() {
    this.props.like();
  }

  render() {
    const {
      name,
      username,
      thumb_url,
      asset_id,
      preview_original_asset_url
    } = this.props.data;
    let avatar;
    let teste;
    let count = 0;

    if (this.state.color) {
      teste = "blue";
      count += 1;
    } else {
      count -= 1;
      teste = "red";
    }

    if (thumb_url == null) {
      avatar =
        "https://mario.aminoapps.com/static/img/user-icon-placeholder.png";
    } else {
      avatar = `${url}${thumb_url}`;
    }
    return (
      <View style={styles.feedContainer}>
        <View style={styles.feedHeader}>
          <View style={styles.avatar}>
            <Image source={{ uri: avatar }} style={styles.userImage} />
          </View>
          <View style={styles.userName}>
            <Text style={styles.userNameText}>{count}</Text>
          </View>
          <View style={styles.dateArea}>
            <View style={styles.postDate}>
              <TouchableOpacity
                style={{ flex: 1, backgroundColor: "red" }}
                onPress={() => this.setState({ color: !this.state.color })}
              />
            </View>
          </View>
        </View>

        <View style={[styles.feedBody, { backgroundColor: teste }]}>
          {/* <Image
            source={{
              uri: `${url_assets}${asset_id}/${preview_original_asset_url}`
            }}
            style={styles.feedImage}
          /> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  feedContainer: {
    width: "100%",
    height: 300
    //backgroundColor: "grey"
  },
  feedHeader: {
    height: 70,
    flexDirection: "row",
    alignItems: "center"
    //backgroundColor: "black"
  },
  avatar: {
    width: 40,
    height: 40,
    marginLeft: 10,
    marginRight: 15
    //backgroundColor: "grey"
  },
  userImage: {
    height: 40,
    width: 40
  },
  userName: {
    width: 150,
    height: 15,
    justifyContent: "center"
    //backgroundColor: "grey"
  },
  userNameText: {
    fontWeight: Platform.OS === "ios" ? "bold" : "normal"
  },
  dateArea: {
    flex: 1,
    //backgroundColor: "red",
    alignItems: "flex-end"
  },
  postDate: {
    width: 80,
    height: 15,
    backgroundColor: "grey",
    marginRight: 10
  },
  feedBody: {
    flex: 1
  },
  feedImage: {
    height: 300,
    width: 300
  }
});
