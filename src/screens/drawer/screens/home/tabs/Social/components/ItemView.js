import React, { PureComponent } from "react";
import { View, Image, Dimensions, ActivityIndicator } from "react-native";
//import Video from "../../../../../../../../../../Library/Caches/typescript/2.9/node_modules/@types/react-native-video";
import { Ionicons } from "@expo/vector-icons";

const { width } = Dimensions.get("window");

export default class itemView extends PureComponent {
  state = {
    rate: 1,
    volume: 1,
    muted: false,
    resizeMode: "contain",
    duration: 0.0,
    currentTime: 0.0,
    controls: false,
    paused: true,
    skin: "custom",
    ignoreSilentSwitch: null,
    isBuffering: false,
    imageHeight: 0,
    message: this.props.cutmessage,
    changecolor: "gray",
    isReady: false
  };

  constructor(props) {
    super(props);
    // this.onLoad = this.onLoad.bind(this);
    // this.onProgress = this.onProgress.bind(this);
    // this.onBuffer = this.onBuffer.bind(this);
  }

  // onLoad(data) {
  //   this.setState({ duration: data.duration });
  // }

  // onProgress(data) {
  //   this.setState({ currentTime: data.currentTime });
  // }

  // onBuffer({ isBuffering }: { isBuffering: boolean }) {
  //   this.setState({ isBuffering });
  // }

  render() {
    if (this.props.type === "image") {
      if (this.props.hasimage != null) {
        Image.getSize(this.props.source, (w, h) => {
          this.setState({
            imageHeight: Math.floor(h * (width / w)),
            isReady: true
          });
        });

        return (
          <View>
            {this.state.isReady === true && (
              <Image
                source={{ uri: this.props.source }}
                style={{ width, height: this.state.imageHeight }}
                resizeMode={"contain"}
              />
            )}

            {this.state.isReady === false && (
              <ActivityIndicator size="large" color="#197db3" />
            )}
          </View>
        );
      } else {
        return (
          <View style={{ backgroundColor: "#f9f9f9" }}>
            <Image
              source={require("../../../../../../../../assets/img/MegaFonePlaces.png")}
              style={{ width, height: 230 }}
              resizeMode={"contain"}
            />
          </View>
        );
      }
    }

    if (this.props.type === "pdf") {
      return (
        <View style={{ backgroundColor: "#e35b50" }}>
          <Image
            source={require("../../../../../../../../assets/img/placesPdf.png")}
            style={{ width, height: 230 }}
            resizeMode={"contain"}
          />
        </View>
      );
    }

    if (this.props.type === "excel") {
      return (
        <View style={{ backgroundColor: "#539271" }}>
          <Image
            source={require("../../../../../../../../assets/img/imageExcel.png")}
            style={{ width, height: 230 }}
            resizeMode={"contain"}
          />
        </View>
      );
    }
  }
}

// <TouchableOpacity
//   onPress={() => {
//     this.setState({ paused: !this.state.paused });
//   }}
//   activeOpacity={0.8}
//   style={{ width, height: width }}
// >
//   <Video
//     source={{ uri: this.props.source }}
//     style={{ width, height: width }}
//     rate={this.state.rate}
//     paused={this.state.paused}
//     volume={this.state.volume}
//     muted={this.state.muted}
//     ignoreSilentSwitch={this.state.ignoreSilentSwitch}
//     resizeMode={"cover"}
//     onLoad={this.onLoad}
//     onBuffer={this.onBuffer}
//     onProgress={this.onProgress}
//     onEnd={() => null}
//     repeat={true}
//   />
//   <View
//     style={{
//       position: "absolute",
//       right: 10,
//       top: 10,
//       backgroundColor: "rgba(0, 0, 0, 0.6)",
//       height: 40,
//       width: 40,
//       borderRadius: 20
//     }}
//   >
//     <Ionicons
//       name="ios-videocam-outline"
//       size={24}
//       color="white"
//       style={{
//         backgroundColor: "transparent",
//         lineHeight: 40,
//         marginLeft: 10
//       }}
//     />
//   </View>
// </TouchableOpacity>
