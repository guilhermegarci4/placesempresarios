import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";

import Comments from "../Comments";
import Posts from "../components/Posts";
import VisitProfile from "../VisitProfile";
import Notifications from "./Notifications";
import ViewPublication from "./ViewPublication";
import ListNotification from "../components/ListNotification";

const NotificationNavigator = createStackNavigator(
  {
    Notifications: {
      screen: Notifications
    },
    Comments: {
      screen: Comments
    },
    Posts: {
      screen: Posts
    },
    VisitProfile: {
      screen: VisitProfile
    },
    ViewPublication: {
      screen: ViewPublication
    },
    ListNotification: {
      screen: ListNotification
    }
  },
  {
    initialRouteName: "Notifications",
    navigationOptions: {
      header: null
    }
  }
);

export default NotificationNavigator;
