import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  Platform,
  StyleSheet,
  Image
} from "react-native";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon,
  List,
  ListItem,
  Button,
  Thumbnail
} from "native-base";
import { observer } from "mobx-react";
import { SocialStore } from "../../../../../../../../Store/SocialStore";
import Posts from "../components/Posts";
import { Constants } from "expo";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

@observer
export default class ViewPublication extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataViewPub: []
    };
  }

  componentDidMount() {
    let viewpub = [];
    SocialStore.dataPosts.find(obj => {
      if (obj.pub_id === this.props.navigation.state.params.idpub) {
        viewpub.push(obj);
      }
    });

    this.setState({ dataViewPub: viewpub });
  }

  render() {
    return (
      <Container>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        {/* <Loading visible={SocialStore.loading} /> */}
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: "#404040" }} />
            </Button>
          </Left>
          <Body
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginLeft: Platform.OS === "ios" ? 0 : "21%"
            }}
          >
            <Image
              source={require("../../../../../../../../assets/logoLogin.png")}
              style={{ width: 100, height: 40 }}
              resizeMode="contain"
            />
          </Body>
          <Right />
        </Header>

        <FlatList
          data={this.state.dataViewPub}
          renderItem={({ item }) => <Posts data={item} />}
          keyExtractor={(item, index) => index.toString()}
          style={{ flex: 1 }}
          //refreshing={this.state.refreshing}
          // onRefresh={this.handleRefresh}
        />
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#FAFAFA",
    height: Constants.statusBarHeight
  }
});
