import React, { Component } from "react";
import { View, Text, FlatList, StyleSheet, Platform } from "react-native";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Icon,
  List,
  ListItem,
  Button,
  Thumbnail
} from "native-base";
import { observer } from "mobx-react";
import { Auth } from "../../../../../../../../Store/Auth";
import { SocialStore } from "../../../../../../../../Store/SocialStore";
import ListNotification from "../components/ListNotification";
import { Constants } from "expo";
import { FontAwesome } from "@expo/vector-icons";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

@observer
export default class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      instance: ""
    };
  }

  CloseModalNotification() {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <Container>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left />
          <Body>
            <Title style={{ color: "#404040" }}>Notificações</Title>
          </Body>
          <Right>
            <Button
              transparent
              onPress={this.CloseModalNotification.bind(this)}
            >
              <FontAwesome name="window-close" size={23} color="#404040" />
            </Button>
          </Right>
        </Header>
        <Content>
          <View style={{ flex: 1 }}>
            <List>
              <FlatList
                data={SocialStore.listStoryNotification}
                renderItem={({ item }) => <ListNotification data={item} />}
                keyExtractor={(item, index) => index.toString()}
              />
            </List>
          </View>
        </Content>
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#FAFAFA",
    height: Constants.statusBarHeight
  }
});
