import React, { Component } from "react";
import {
  View,
  StatusBar,
  StyleSheet,
  Platform,
  WebView,
  Text
} from "react-native";
import { Header, Container, Left, Right, Body, Button } from "native-base";
import { Constants } from "expo";
import { MaterialIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import Loading from "../../../../../components/loading";
import { PortalStore } from "../../../../../../Store/PortalStore";

Platform.select({
  ios: () => StatusBar.setBarStyle("light-content"),
  android: () => StatusBar.setBackgroundColor("#263238")
})();

@observer
export default class Portal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    window.globalNavigator = this.props.navigation;
    this.OpenDrawer = this.OpenDrawer.bind(this);
    this.ShowPortal = this.ShowPortal.bind(this);
    //this.LoadError = this.LoadError.bind(this);
  }

  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  componentWillMount() {
    PortalStore.setLoading(true);
  }

  ShowPortal() {
    PortalStore.setLoading(false);
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fafafa" }}>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={this.OpenDrawer}>
              <MaterialIcons name="dehaze" size={25} color="#404040" />
            </Button>
          </Left>
          <Body
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginLeft: Platform.OS === "ios" ? 0 : "21%"
            }}
          >
            <Text
              style={{
                marginRight: 4,
                fontWeight: "bold",
                fontSize: 16,
                color: "#404040"
              }}
            >
              Site
            </Text>
          </Body>
          <Right />
        </Header>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Loading visible={PortalStore.loading} />
        <WebView
          source={{ uri: "https://www.usjt.br" }}
          onLoadStart={this.ShowPortal}
        />
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#FAFAFA",
    height: Constants.statusBarHeight
  }
});
