// @flow
import * as React from "react";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Text
} from "react-native";
import { Constants } from "expo";
import { Icon } from "native-base";

import { StyleGuide, type StyleProps } from "./theme";

export type SheetProps = StyleProps & {
  title: string,
  subtitle?: string,
  children: React.Node,
  noSafeArea: boolean,
  scrollable: boolean,
  rightAction?: {
    label: string,
    onPress: () => void
  },
  toggle: () => mixed
};

export default class Sheet extends React.PureComponent<SheetProps> {
  static defaultProps = {
    noSafeArea: false,
    scrollable: false
  };

  render(): React.Node {
    const {
      style,
      toggle,
      title,
      subtitle,
      rightAction,
      noSafeArea,
      scrollable
    } = this.props;
    const children = noSafeArea ? (
      this.props.children
    ) : (
      <SafeAreaView>{this.props.children}</SafeAreaView>
    );
    return (
      <View style={[styles.content, style]}>
        <TouchableWithoutFeedback onPress={toggle}>
          <View style={styles.header}>
            <TouchableOpacity style={styles.left} onPress={toggle}>
              <Text>Cancelar</Text>
            </TouchableOpacity>
            <View style={styles.center}>
              <Text style={styles.title}>{title}</Text>
              {subtitle && <Text style={styles.title}>{subtitle}</Text>}
            </View>
            <View style={styles.right}>
              {rightAction && (
                <TouchableOpacity onPress={rightAction.onPress}>
                  <Text style={styles.postbtn}>{rightAction.label}</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
        {scrollable ? (
          <ScrollView bounces={false}>{children}</ScrollView>
        ) : (
          children
        )}
      </View>
    );
  }
}

const { height } = Dimensions.get("window");
const styles = StyleSheet.create({
  content: {
    backgroundColor: StyleGuide.palette.white,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    maxHeight: height - Constants.statusBarHeight
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: StyleGuide.spacing.small,
    paddingVertical: StyleGuide.spacing.tiny,
    borderBottomWidth: 1,
    borderColor: StyleGuide.palette.lightGray
  },
  left: {
    alignItems: "center",
    justifyContent: "center"
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    textAlign: "center",
    fontSize: 16
  },
  right: {
    minWidth: 36,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  postbtn: {
    fontWeight: "bold",
    color: "#209cdf",
    padding: 10,
    fontSize: 15
  }
});
