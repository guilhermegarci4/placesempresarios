// @flow
import autobind from "autobind-decorator";
import * as React from "react";
import { observable, action } from "mobx";
import { observer } from "mobx-react/native";
import {
  StyleSheet,
  TextInput,
  View,
  Button,
  TouchableOpacity,
  Text,
  Image
} from "react-native";
import KeyboardSpacer from "./KeyboardSpacer";
import ActionSheet from "react-native-actionsheet";
import { Constants, ImagePicker, Permissions, ImageManipulator } from "expo";

import { StyleGuide } from "./theme";
import { PublishStore } from "../../../../../../../../Store/PublishStore";

@observer
export default class NewMessage extends React.Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      postPhoto:
        "https://mario.aminoapps.com/static/img/user-icon-placeholder.png",
      largura: 600
    };

    this.cleanImage = this.cleanImage.bind(this);
  }

  cleanImage() {
    this.setState({ visible: false, postPhoto: "" });
    PublishStore.setImg(null);
  }

  itemImage(visible) {
    if (!visible) {
      return (
        <View style={{ alignSelf: "center" }}>
          <TouchableOpacity onPress={this.showActionSheet}>
            <Text style={{ color: "#197db3" }}>Adicionar Imagem</Text>
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={{ alignSelf: "center" }}>
        <Image
          source={{
            uri: this.state.postPhoto
          }}
          style={{
            alignSelf: "center",
            width: 100,
            height: 80
          }}
          resizeMode="contain"
        />
        <TouchableOpacity
          onPress={this.cleanImage}
          style={{ margin: 10, alignSelf: "center" }}
        >
          <Text style={{ color: "#197db3" }}>Remover Imagem</Text>
        </TouchableOpacity>
      </View>
    );
  }

  pickFromGallery = async () => {
    const permissions = Permissions.CAMERA_ROLL;
    const { status } = await Permissions.askAsync(permissions);

    if (status === "granted") {
      let image = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: "Images",
        allowsEditing: false,
        aspect: [4, 3],
        base64: true
        //quality: 0.2
      }).catch(error => console.log(permissions, { error }));

      if (!image.cancelled) {
        this.setState({ postPhoto: image.uri, visible: true });

        Image.getSize(image.uri, (w, h) => {
          if (w < 600) {
            this.setState({ largura: w });
          }
        });

        ImageManipulator.manipulate(
          image.uri,
          [{ resize: { width: this.state.largura } }],
          {
            base64: true,
            compress: 0.5
          }
        ).then(img => {
          PublishStore.setImg(img.base64);
        });
      }
    }
  };

  pickFromCamera = async () => {
    const permissions = Permissions.CAMERA;
    const { status } = await Permissions.askAsync(permissions);

    if (status === "granted") {
      let image = await ImagePicker.launchCameraAsync({
        mediaTypes: "Images",
        allowsEditing: false,
        aspect: [4, 3],
        base64: true
      }).catch(error => console.log(permissions, { error }));

      if (!image.cancelled) {
        this.setState({ postPhoto: image.uri, visible: true });
        Image.getSize(image.uri, (w, h) => {
          if (w < 600) {
            this.setState({ largura: w });
          }
        });

        ImageManipulator.manipulate(
          image.uri,
          [{ resize: { width: this.state.largura } }],
          {
            base64: true,
            compress: 0.5
          }
        ).then(img => {
          PublishStore.setImg(img.base64);
        });
      }
    }
  };

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  selectActionSheetAction(index) {
    if (index == "0") this.pickFromGallery();

    if (index == "1") this.pickFromCamera();
  }

  render(): React.Node {
    return (
      <View style={styles.container}>
        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          title={"De onde deseja escolher sua foto?"}
          options={["Galeria", "Câmera", "Cancelar"]}
          cancelButtonIndex={2}
          onPress={index => {
            this.selectActionSheetAction(index);
          }}
        />
        <TextInput
          style={styles.textInput}
          placeholder="No que você está pensando ?"
          underlineColorAndroid="transparent"
          textAlignVertical="top"
          onChangeText={txt => PublishStore.setTextPost(txt)}
          value={PublishStore.textPost}
          multiline
          autoFocus
        />

        {this.itemImage(this.state.visible)}
        <KeyboardSpacer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: StyleGuide.spacing.base
  },
  textInput: {
    height: 143,
    ...StyleGuide.typography.body
  }
});
