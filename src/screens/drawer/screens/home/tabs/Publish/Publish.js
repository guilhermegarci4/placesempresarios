import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  Image,
  TextInput,
  Alert
} from "react-native";
import {
  Container,
  Content,
  Header,
  Right,
  Left,
  Body,
  Button,
  Card,
  CardItem,
  Thumbnail,
  Input,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import { Constants } from "expo";
import { observer } from "mobx-react";
import { Auth } from "../../../../../../../Store/Auth";
import { PublishStore } from "../../../../../../../Store/PublishStore";
import Loading from "../../../../../../components/loading";
import autobind from "autobind-decorator";
import ActionSheet from "./components/ActionSheet";
import NewMessage from "./components/NewMessage";

const url =
  "https://s3-sa-east-1.amazonaws.com/fc-exclusive/uploads/social/profiles/";

@observer
export default class Publish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textPublish: "",
      loading: false,
      visible: false
    };

    this.showActionSheet = this.showActionSheet.bind(this);
  }

  @autobind
  onPress() {
    this.newPost.toggle();
  }

  newPost: ActionSheet;

  @autobind
  newPostRef(newPost: ActionSheet | null) {
    if (newPost) {
      this.newPost = newPost;
    }
  }

  Publicar() {
    this.setState({ loading: true });
    PublishStore.publish(this.state.textPublish, res => {
      if (res) {
        this.setState({ loading: false });
        setTimeout(() => {
          Alert.alert("Sucesso", "Sua publicação foi realizada!", [
            { text: "OK", onPress: () => console.log("fechou") }
          ]);
        }, 300);
      } else {
        this.setState({ loading: false });
        setTimeout(() => {
          Alert.alert(
            "Erro",
            "Não foi possível realizar a publicação, por favor tente mais tarde.",
            [{ text: "OK", onPress: () => console.log("fechou") }]
          );
        }, 300);
      }
    });
  }

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  selectActionSheetAction(index) {
    if (index == "0") {
      console.log("escolheu 0");
    }

    if (index == "1") {
      console.log("escolheu 1");
    }
  }

  render() {
    let avatar = "";
    if (Auth.ProfilePhoto === null) {
      avatar = null;
    } else {
      avatar = `${url}${Auth.ProfilePhoto}`;
    }

    const { onPress } = this;
    const { navigation } = this.props;
    const rightAction = {
      icon: "edit",
      onPress
    };
    const postAction = {
      label: "Post",
      onPress: () => alert("teste")
    };
    return (
      <Container>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Loading visible={this.state.loading} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent>
              <Ionicons name="ios-menu" size={25} color="black" />
            </Button>
          </Left>
          <Body style={{ marginLeft: Platform.OS === "ios" ? 0 : 50 }}>
            <Image
              source={require("../../../../../../../assets/img/logonew.png")}
              style={{ width: 60, height: 35 }}
              resizeMode="contain"
            />
          </Body>
          <Right />
        </Header>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail
                  source={{
                    uri:
                      avatar ||
                      "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
                  }}
                />
                <Body>
                  <Input
                    placeholder="No que você está pensando ?"
                    placeholderTextColor="gray"
                    multiline={true}
                    value={this.state.textPublish}
                    onChangeText={txt => {
                      this.setState({ textPublish: txt });
                    }}
                    style={{
                      fontSize: 15,
                      borderWidth: 0.5,
                      borderColor: "#a6a6a6",
                      borderRadius: 5
                    }}
                  />
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Left />
              <Body />
              <Right>
                <Button
                  transparent
                  style={{
                    paddingLeft: 5,
                    paddingRight: 5
                  }}
                  onPress={this.Publicar.bind(this)}
                >
                  <Text style={{ color: "#000", fontWeight: "bold" }}>
                    Compartilhar
                  </Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
          <ActionSheet
            title="Nova Postagem"
            ref={this.newPostRef}
            rightAction={postAction}
          >
            <NewMessage />
          </ActionSheet>
          <Button onPress={() => this.newPost.toggle()}>
            <Text>TESTE</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#fafafa",
    height: Constants.statusBarHeight
  }
});
