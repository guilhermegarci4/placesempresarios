import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";

import Publish from "./Publish";

const PublishNavigator = createStackNavigator(
  {
    Publish: {
      screen: Publish
    }
  },
  {
    initialRouteName: "Publish",
    navigationOptions: {
      header: null
    }
  }
);

export default PublishNavigator;
