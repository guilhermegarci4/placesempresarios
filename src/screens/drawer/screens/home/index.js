import React, { Component } from "react";
import {
  View,
  Alert,
  TouchableWithoutFeedback,
  StyleSheet,
  Platform,
  Linking,
  Image
} from "react-native";
import { createBottomTabNavigator, BottomTabBar } from "react-navigation";
import {
  Feather,
  MaterialIcons,
  MaterialCommunityIcons
} from "@expo/vector-icons";
import { Auth } from "../../../../../Store/Auth";
import { PublishStore } from "../../../../../Store/PublishStore";
import { observer } from "mobx-react";

import ActionSheet from "./tabs/Publish/components/ActionSheet";
import autobind from "autobind-decorator";
import NewMessage from "./tabs/Publish/components/NewMessage";

import Social from "./tabs/Social/index";
import Profile from "./tabs/Profile/profile";
import Portal from "./tabs/Portal";

@observer
class IconName extends Component {
  render() {
    let iconName;
    let colorIcon;
    if (this.props.goto === "Social") {
      iconName = "home";
      colorIcon = this.props.focused ? "#209cdf" : "gray";
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <MaterialIcons name={iconName} size={30} color={colorIcon} />
        </View>
      );
    } else if (this.props.goto === "Portal") {
      iconName = "library-books";
      colorIcon = this.props.focused ? "#209cdf" : "gray";
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <MaterialIcons name={iconName} size={25} color={colorIcon} />
        </View>
      );
    } else if (this.props.goto === "Publish") {
      iconName = "control-point";
      colorIcon = this.props.focused ? "#209cdf" : "gray";
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <MaterialIcons name={iconName} size={30} color={colorIcon} />
        </View>
      );
    } else if (this.props.goto === "Profile" && Platform.OS === "android") {
      iconName = "person";
      colorIcon = this.props.focused ? "#209cdf" : "gray";
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <MaterialIcons name={iconName} size={30} color={colorIcon} />
        </View>
      );
    } else if (this.props.goto === "VirtusApp") {
      iconName = "box";
      colorIcon = this.props.focused ? "#209cdf" : "gray";
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Feather name={iconName} size={25} color={colorIcon} />
        </View>
      );
    }

    if (this.props.goto === "Profile" && Platform.OS === "ios") {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View
            style={{
              borderWidth: 1,
              borderColor: this.props.focused ? "#209cdf" : "transparent",
              borderRadius: 15,
              height: 30,
              width: 30,
              alignItems: "center",
              justifyContent: "center",
              padding: 3
            }}
          >
            <Image
              source={{
                uri:
                  Auth.ProfilePhoto ||
                  "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
              }}
              style={{ height: 25, width: 25, borderRadius: 12.5 }}
              resizeMode="cover"
            />
          </View>
        </View>
      );
    }
  }
}

class TabBar extends Component {
  constructor(props) {
    super(props);
    this.goToStore = this.goToStore.bind(this);
  }
  renderItem = (route, index) => {
    const { navigation, jumpToIndex } = this.props;

    // const isResgatar = route.routeName === "VirtusApp";

    const focused = index === navigation.state.index;
    const color = focused ? activeTintColor : inactiveTintColor;

    let rName = "";
    if (route.routeName === "Social") {
      rName = "Social";
    } else if (route.routeName === "Portal") {
      rName = "Portal";
    } else if (route.routeName === "Publish") {
      rName = "Publicar";
    } else if (route.routeName === "Profile") {
      rName = "Perfil";
    } else if (route.routeName === "VirtusApp") {
      rName = "Resgatar";
    }
    return (
      <TouchableWithoutFeedback
        key={route.key}
        style={styles.tab}
        onPress={() => {
          if (rName === "Social") {
            navigation.navigate(route.routeName);
          }
          if (rName === "Portal") {
            navigation.navigate(route.routeName);
          }
          if (rName === "Publicar") {
            this.newPost.toggle();
          }

          if (rName === "Perfil") {
            navigation.navigate(route.routeName);
          }
          if (rName === "Resgatar") {
            this.goToStore();
          }
        }}
      >
        <View style={styles.tab}>
          <IconName goto={route.routeName} focused={focused} />
          {/* <Text style={{ color, fontSize: 11, marginBottom: 2 }}>{rName}</Text> */}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  goToStore() {
    if (Platform.OS === "ios") {
      Linking.openURL(
        "https://itunes.apple.com/us/app/virtus-app/id1345728985?platform=iphone&preserveScrollPosition=true#platform/iphone"
      );
    } else {
      Linking.openURL(
        "https://play.google.com/store/apps/details?id=com.clubvirtus.app"
      );
    }
  }

  @autobind
  onPress() {
    this.newPost.toggle();
  }

  newPost: ActionSheet;

  @autobind
  newPostRef(newPost: ActionSheet | null) {
    if (newPost) {
      this.newPost = newPost;
    }
  }

  PublicPost() {
    if (PublishStore.textPost.length > 0) {
      if (PublishStore.img != null) {
        console.log("ENTREI NO PUBLISH IMAGE");

        PublishStore.publishImage();
      } else {
        console.log("ENTREI NO PUBLISH TEXTO");
        PublishStore.publish();
      }
      this.newPost.toggle();
    } else {
      Alert.alert("Atenção", "Insira algum texto na publicação.", [
        { text: "Ok", onPress: () => console.log("fechou") }
      ]);
    }
  }

  render() {
    const { navigation } = this.props;

    const { routes } = navigation.state;
    const { onPress } = this;

    const postAction = {
      label: "Postar",
      onPress: this.PublicPost.bind(this)
    };

    return (
      <View style={styles.tabBar}>
        {routes && routes.map(this.renderItem)}
        <ActionSheet
          title="Nova Postagem"
          ref={this.newPostRef}
          rightAction={postAction}
        >
          <NewMessage />
        </ActionSheet>
      </View>
    );
  }
}

const Navegador = createBottomTabNavigator(
  {
    Portal: {
      screen: Portal
    },
    Social: {
      screen: Social
    },
    // Publish: {
    //   screen: <View />
    // },
    Profile: {
      screen: Profile
    },
    // VirtusApp: {
    //   screen: View
    // }
  },
  {
    initialRouteName: "Social",
    tabBarComponent: TabBar,
    tabBarPosition: "bottom",

    animationEnabled: false,
    swipeEnabled: false
  }
);

const activeTintColor = "black";
const inactiveTintColor = "#939393";

const styles = StyleSheet.create({
  tabBar: {
    height: 49,
    flexDirection: "row",
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: "rgba(0, 0, 0, .3)",
    backgroundColor: "#fafafa"
  },
  tab: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-end"
  }
});
export default Navegador;
