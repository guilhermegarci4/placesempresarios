import React, { Component, PureComponent } from "react";
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Platform,
  Image,
  FlatList,
  Text,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Card,
  CardItem,
  Button,
  Left,
  Body,
  Right
} from "native-base";
import { Constants } from "expo";
import { MaterialIcons } from "@expo/vector-icons";
import Loading from "../../../../components/loading";

const { width } = Dimensions.get("window");

import barbearia from "../../../../../assets/img/barbeariaze.jpeg";
import spazio from "../../../../../assets/img/logospazio.png";
import mpj from "../../../../../assets/img/MPJ.png";
import becco from "../../../../../assets/img/beccoburguer.png";
import embarque from "../../../../../assets/img/logoea.png";
import polihouse from "../../../../../assets/img/logopolihouse.png";

export default class Parceiros extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      imageHeight: 0,
      parceiros: [
        {
          id: 1,
          nomeFantasia: "Embarque Agora",
          image:
            "https://firebasestorage.googleapis.com/v0/b/checkpoint-86b9d.appspot.com/o/saojudas%2Flogoea.png?alt=media&token=99dbd2e1-ffb8-484b-92f1-751178eeb167",
          endereco: "www.embarqueagora.com.br",
          cidade: "Agência de Turismo e Eventos",
          fone: "(15) 3244.2607",
          ecommerce: true
        },
        {
          id: 2,
          nomeFantasia: "Polihouse",
          image:
            "https://firebasestorage.googleapis.com/v0/b/checkpoint-86b9d.appspot.com/o/saojudas%2Flogopolihouse.png?alt=media&token=915ec527-547b-435f-8fc0-e485ef9d86bc",
          endereco: "www.polihouse.com.br/virtus",
          cidade: "Esporte e Lazer",
          fone: "(19) 3701.3956",
          ecommerce: true
        },
        {
          id: 3,
          nomeFantasia: "Barbearia do Zé",
          image:
            "https://firebasestorage.googleapis.com/v0/b/checkpoint-86b9d.appspot.com/o/saojudas%2Fbarbeariaze.png?alt=media&token=54c38bc4-66ac-40ec-95dc-c2bdc1336a5b",

          endereco: "Av. Casa Grande, 1633 - Casa Grande",
          cidade: "Diadema",
          fone: "(11) 4066.2541",
          ecommerce: false
        },
        {
          id: 4,
          nomeFantasia: "Barbearia do Zé",
          image:
            "https://firebasestorage.googleapis.com/v0/b/checkpoint-86b9d.appspot.com/o/saojudas%2Fbarbeariaze.png?alt=media&token=54c38bc4-66ac-40ec-95dc-c2bdc1336a5b",

          endereco: "Av. José Odorizzi, 2169 - Assunção",
          cidade: "São Bernardo do Campo",
          fone: "(11) 2669.7416",
          ecommerce: false
        },
        {
          id: 5,
          nomeFantasia: "Becco 7 Burgger",
          image:
            "https://firebasestorage.googleapis.com/v0/b/checkpoint-86b9d.appspot.com/o/saojudas%2Fbeccoburguer.png?alt=media&token=77bdee31-3687-4760-9997-ef928bd250f5",
          endereco: "Rua Amaro Genari, 46 - Assunção",
          cidade: "São Bernardo do Campo",
          fone: "(11) 4509.5539",
          ecommerce: false
        },
        {
          id: 6,
          nomeFantasia: "Spazio Luce",
          image:
            "https://firebasestorage.googleapis.com/v0/b/checkpoint-86b9d.appspot.com/o/saojudas%2Flogospazio.png?alt=media&token=999c5311-66c0-4a2d-ad0a-b485d60df190",
          endereco: "Rua Olavio Bilac, 299 - Vila Santana",
          cidade: "Sorocaba",
          fone: "(15) 3342.6164",
          ecommerce: false
        },
        {
          id: 7,
          nomeFantasia: "MPJ Construção Industrial",
          image:
            "https://firebasestorage.googleapis.com/v0/b/checkpoint-86b9d.appspot.com/o/saojudas%2FMPJ.png?alt=media&token=c14d350f-e9ab-4c65-9837-c16d8ddd25d5",
          endereco: "R. Abel Souto, 87 - Vila Santa Luzia",
          cidade: "Alumínio",
          fone: "(11) 4715.0492",
          ecommerce: false
        }
      ],
      isReady: false
    };
    window.globalNavigator = this.props.navigation;
    this.ShowPortal = this.ShowPortal.bind(this);
    this.OpenDrawer = this.OpenDrawer.bind(this);
    //this.LoadError = this.LoadError.bind(this);
  }

  ShowPortal() {
    this.setState({ loading: false });
  }

  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  CardItem(item) {
    return (
      <View>
        <Card>
          <CardItem>
            <Left>
              <Body>
                <Text style={{ fontWeight: "bold", color: "#404040" }}>
                  {item.nomeFantasia}
                </Text>
                <Text note style={{ color: "gray" }}>
                  Parceiro
                </Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem cardBody>
            <Image
              source={{ uri: item.image }}
              style={{ height: 250, width }}
              resizeMode="contain"
            />
          </CardItem>
          <CardItem>
            <View
              style={{
                // borderWidth: 1,
                // borderColor: "red",
                //height: 100,
                flex: 1
              }}
            >
              <Text style={{ fontWeight: "bold", color: "#404040" }}>
                {item.ecommerce ? "Site" : "Endereço"}
              </Text>
              <Text>{item.endereco}</Text>

              <Text
                style={{ fontWeight: "bold", marginTop: 10, color: "#404040" }}
              >
                {item.ecommerce ? "" : "Cidade"}
              </Text>
              <Text>{item.cidade}</Text>

              <Text
                style={{ fontWeight: "bold", marginTop: 10, color: "#404040" }}
              >
                Contato
              </Text>
              <Text>{item.fone}</Text>
            </View>
            {/* <Left>
                
                <Text>{item.endereco}</Text>
              
            </Left> */}
            {/* <Body>
              <Button transparent>
                <Text>{item.cidade}</Text>
              </Button>
            </Body> */}
            {/* <Right>
              <Text>{item.fone}</Text>
            </Right> */}
          </CardItem>
        </Card>
      </View>
    );
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fafafa" }}>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Loading visible={this.state.loading} />
        <Header style={{ backgroundColor: "#fafafa" }}>
          <Left>
            <Button transparent onPress={this.OpenDrawer}>
              <MaterialIcons name="dehaze" size={25} color="#404040" />
            </Button>
          </Left>
          <Body
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginLeft: Platform.OS === "ios" ? 0 : "21%"
            }}
          >
            <Image
              source={require("../../../../../assets/logoLetter.png")}
              style={{
                width: 100,
                height: 40
              }}
              resizeMode="contain"
            />
          </Body>
          <Right />
        </Header>
        <FlatList
          data={this.state.parceiros}
          renderItem={({ item }) => this.CardItem(item)}
          keyExtractor={item => item.id.toString()}
          style={{ flex: 1 }}
        />
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#rgb(232,9,17)",
    height: Constants.statusBarHeight
  }
});
