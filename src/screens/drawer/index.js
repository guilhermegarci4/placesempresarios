import React, { Component } from "react";
import { createDrawerNavigator } from "react-navigation";
import {
  TouchableOpacity,
  StyleSheet,
  Platform,
  AsyncStorage,
  Linking
} from "react-native";
import {
  Container,
  Text,
  View,
  Right,
  Left,
  Body,
  Button,
  List,
  ListItem,
  Thumbnail,
  Accordion
} from "native-base";
import {
  SimpleLineIcons,
  MaterialIcons,
  FontAwesome,
  MaterialCommunityIcons
} from "@expo/vector-icons";
import { Auth } from "../../../Store/Auth";
import { LinearGradient, Constants } from "expo";

import Home from "./screens/home";
import Ranking from "./screens/home/tabs/Ranking";
import Site from "./screens/Site/Site";
import Virtus from "./screens/Virtus/Virtus";
import Parceria from "./screens/Parceria/Parceria";
import Suporte from "./screens/Suporte/Suporte";
import Parceiros from "./screens/Parceiros/Parceiros";

class MenuParceiros extends Component {
  constructor(props) {
    super(props);
    this._handleOpenStore = this._handleOpenStore.bind(this);
    this.OpenVirtus = this.OpenVirtus.bind(this);
    this.OpenParceria = this.OpenParceria.bind(this);
    this.OpenParceiros = this.OpenParceiros.bind(this);
  }

  _handleOpenStore() {
    if (Platform.OS === "ios") {
      Linking.openURL(
        "https://itunes.apple.com/us/app/virtus-app/id1345728985?platform=iphone&preserveScrollPosition=true#platform/iphone"
      );
    } else {
      Linking.openURL(
        "https://play.google.com/store/apps/details?id=com.clubvirtus.app"
      );
    }
  }

  OpenVirtus() {
    window.globalNavigator.navigate("Virtus");
  }

  OpenParceria() {
    window.globalNavigator.navigate("Parceria");
  }

  OpenParceiros() {
    window.globalNavigator.navigate("Parceiros");
  }

  render() {
    return (
      <View style={{ paddingLeft: "10%" }}>
        <TouchableOpacity
          style={{ marginBottom: 10, marginTop: 10 }}
          onPress={this.OpenParceiros}
        >
          <Text>Parceiros</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginBottom: 10, marginTop: 10 }}
          onPress={this._handleOpenStore}
        >
          <Text>Vantagens</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginBottom: 10, marginTop: 10 }}
          onPress={this.OpenParceria}
        >
          <Text>Seja Parceiro</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginBottom: 10, marginTop: 10 }}
          onPress={this.OpenVirtus}
        >
          <Text>Sobre a Virtus</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const dataArray = [
  {
    title: "Mais..."
  }
];

class CustomDrawerContentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.ButtonRanking = this.ButtonRanking.bind(this);
    this.ButtonLogout = this.ButtonLogout.bind(this);
    this.ButtonMural = this.ButtonMural.bind(this);
    this.OpenDrawer = this.OpenDrawer.bind(this);
    this._handleOpenSite = this._handleOpenSite.bind(this);
    this._handleOpenFB = this._handleOpenFB.bind(this);
    this.OpenSuporte = this.OpenSuporte.bind(this);
    this.handleIngressos = this.handleIngressos.bind(this);
  }

  OpenDrawer() {
    this.props.navigation.toggleDrawer();
  }

  OpenSuporte() {
    window.globalNavigator.navigate("Suporte");
  }

  ButtonLogout() {
    AsyncStorage.clear();
    window.globalNavigator.navigate("SignIn");
  }

  ButtonRanking() {
    window.globalNavigator.navigate("Ranking");
  }

  ButtonMural() {
    window.globalNavigator.navigate("Home");
  }

  _handleOpenSite() {
    window.globalNavigator.navigate("Site");
  }

  _handleOpenFB() {
    Linking.openURL("https://www.facebook.com/usjtoficial/");
  }

  handleIngressos() {
    Linking.openURL("https://www.sympla.com.br/saojudasvoleibol");
  }

  render() {
    return (
      <Container>
        {/* <DrawerItems {...this.props} /> */}

        <View style={{ flex: 1 }}>
          <View style={Platform.OS === "ios" ? {} : types.statusBar} />
          <View style={{ height: Platform.OS === "ios" ? "20%" : "22%" }}>
            <LinearGradient colors={["#0d3e59", "#197db3"]} style={{ flex: 1 }}>
              <View
                style={{
                  flex: 1,
                  padding: 10
                }}
              />
              <View
                style={{
                  flex: 3,
                  //padding: 10,
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Thumbnail
                  source={{
                    uri:
                      Auth.ProfilePhoto ||
                      "https://mario.aminoapps.com/static/img/user-icon-placeholder.png"
                  }}
                  style={{ marginLeft: 5 }}
                />
                <TouchableOpacity onPress={this.OpenDrawer}>
                  <FontAwesome
                    name="reply"
                    size={23}
                    color="#fff"
                    style={{ alignSelf: "flex-end", marginRight: 15 }}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 2,
                  //padding: 10,
                  flexDirection: "row"
                }}
              >
                <View
                  style={{
                    paddingTop: 2,
                    paddingLeft: 8,

                    flex: 1
                  }}
                >
                  <Text
                    style={{ color: "#fff", fontWeight: "bold", fontSize: 11 }}
                  >
                    {Auth.name || "Insira seu nome"}
                  </Text>
                  <Text style={{ color: "#fff", fontSize: 8 }}>
                    Voleibol São Judas
                  </Text>
                </View>
                <TouchableOpacity onPress={this._handleOpenFB}>
                  <MaterialCommunityIcons
                    name="facebook-box"
                    size={30}
                    color="#0a2f43"
                    style={{ margin: 4 }}
                  />
                </TouchableOpacity>
              </View>
            </LinearGradient>
          </View>

          <View style={{ flex: 1, paddingTop: 20 }}>
            <List>
              <ListItem icon button onPress={this.ButtonMural}>
                <Left>
                  <Button transparent disabled>
                    <MaterialIcons
                      name="home"
                      size={23}
                      color="#000"
                      style={{ alignSelf: "center" }}
                    />
                  </Button>
                </Left>
                <Body>
                  <Text style={{ color: "#000", fontSize: 16 }}>Início</Text>
                </Body>
                <Right />
              </ListItem>

              <ListItem icon button onPress={this.handleIngressos}>
                <Left>
                  <Button transparent disabled>
                    <FontAwesome
                      name="ticket"
                      size={23}
                      color="#000"
                      style={{ alignSelf: "center" }}
                    />
                  </Button>
                </Left>
                <Body>
                  <Text style={{ color: "#000", fontSize: 16 }}>Ingressos</Text>
                </Body>
                <Right />
              </ListItem>

              <View style={{ flexDirection: "row" }}>
                <View
                  style={{
                    paddingLeft: "8%",
                    paddingRight: Platform.OS === "ios" ? "2%" : "4%"
                  }}
                >
                  <Button transparent>
                    <MaterialIcons
                      name="widgets"
                      size={23}
                      color="#000"
                      style={{ alignSelf: "center" }}
                    />
                  </Button>
                </View>
                <View style={{ flex: 1, paddingTop: "1%" }}>
                  <Accordion
                    dataArray={dataArray}
                    icon="arrow-down"
                    expandedIcon="arrow-up"
                    headerStyle={{
                      backgroundColor: "#fafafa",
                      borderColor: "transparent"
                    }}
                    contentStyle={{ backgroundColor: "#fafafa" }}
                    style={{
                      borderColor: "transparent"
                    }}
                    renderContent={() => <MenuParceiros />}
                  />
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  paddingLeft: Platform.OS === "ios" ? "25%" : "27%"
                }}
              >
                <View
                  style={{
                    borderBottomWidth: 0.5,
                    borderColor: "#bfbfbf"
                  }}
                />
              </View>
              <ListItem icon button onPress={this.OpenSuporte}>
                <Left>
                  <Button transparent disabled>
                    <MaterialIcons
                      name="help"
                      size={23}
                      color="#000"
                      style={{ alignSelf: "center" }}
                    />
                  </Button>
                </Left>
                <Body>
                  <Text style={{ color: "#000", fontSize: 16 }}>Suporte</Text>
                </Body>
                <Right />
              </ListItem>
            </List>
          </View>
          <View style={{ height: "10%" }}>
            <List>
              <ListItem icon onPress={this.ButtonLogout}>
                <Left>
                  <Button transparent>
                    <SimpleLineIcons
                      name="logout"
                      size={23}
                      color="#000"
                      style={{ alignSelf: "center" }}
                    />
                  </Button>
                </Left>
                <Body>
                  <Text style={{ color: "#000", fontSize: 16 }}>Sair</Text>
                </Body>
                <Right />
              </ListItem>
            </List>
          </View>
        </View>
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#0d3e59",
    height: Constants.statusBarHeight
  }
});

const DrawerNavigator = createDrawerNavigator(
  {
    Home: { screen: Home },
    Ranking: { screen: Ranking },
    Site: { screen: Site },
    Suporte: { screen: Suporte },
    Virtus: { screen: Virtus },
    Parceria: { screen: Parceria },
    Parceiros: { screen: Parceiros }
  },
  {
    contentComponent: CustomDrawerContentComponent,
    drawerBackgroundColor: "#fafafa",
    drawerWidth: 240,
    contentOptions: {
      activeTintColor: "#000",
      inactiveTintColor: "#000",
      // activeBackgroundColor:'#fff'
      itemsContainerStyle: {}
    }
  }
);

export default DrawerNavigator;
