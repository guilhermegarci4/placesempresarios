import React, { Component } from "react";
import {
  View,
  StatusBar,
  StyleSheet,
  Platform,
  WebView,
  Text
} from "react-native";
import { Header, Container, Left, Right, Body, Button } from "native-base";
import { Constants } from "expo";
import { MaterialIcons } from "@expo/vector-icons";
import { observer } from "mobx-react";
import { SignUpStore } from "../../../Store/SignUpStore";
import Loading from "../../components/loading";

Platform.select({
  ios: () => StatusBar.setBarStyle("light-content"),
  android: () => StatusBar.setBackgroundColor("#263238")
})();

@observer
export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    window.globalNavigator = this.props.navigation;
    this.ShowCadastro = this.ShowCadastro.bind(this);
    this.voltar = this.voltar.bind(this);
  }

  ShowCadastro() {
    SignUpStore.setLoading(false);
  }

  voltar(){
    this.props.navigation.goBack();
  }

  render() {
    return (
      <Container style={{ backgroundColor: "#fafafa" }}>
      <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Header style={{ backgroundColor: "#fafafa" }}>
        <Left>
            <Button transparent onPress={this.voltar}>
              <MaterialIcons name="keyboard-arrow-left" size={25} color="#404040" />
            </Button>
          </Left>
          <Body
            style={{
              justifyContent: "center",
              alignItems: "center",
              alignSelf: "center",
              marginLeft: Platform.OS === "ios" ? 0 : "21%",
            }}
          >
            <Text
              style={{
                marginRight: 4,
                fontWeight: "bold",
                fontSize: 16,
                color: "#404040"
              }}
            >
              Cadastro
            </Text>
          </Body>
          <Right />
        </Header>
        <View style={Platform.OS === "ios" ? "" : types.statusBar} />
        <Loading visible={SignUpStore.loading} />
        <WebView
          source={{ uri: "https://materiais.sejavirtus.com/socio-torcedor-sao-judas-voleibol" }}
          onLoad={this.ShowCadastro}
        />
      </Container>
    );
  }
}

const types = StyleSheet.create({
  statusBar: {
    backgroundColor: "#FAFAFA",
    height: Constants.statusBarHeight
  }
});
