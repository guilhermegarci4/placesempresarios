import { StyleSheet, Dimensions } from "react-native";
const window = Dimensions.get("window");

export const IMAGE_HEIGHT = window.width / 2;
export const IMAGE_HEIGHT_SMALL = window.width / 7;

export default StyleSheet.create({
  container: {
    alignItems: "center"
  },
  txtinput: {
    height: window.width / 8,
    width: window.width - 100,
    borderColor: "transparent"
  },
  iteminput: {
    height: window.width / 8,
    width: window.width - 100,
    margin: 5,
    borderRadius: 5,
    backgroundColor: "#fafafa"
  },
  logo: {
    width: window.width / 1.7,
    height: IMAGE_HEIGHT,
    resizeMode: "contain"
  }
});
