import React, { Component } from "react";
import {
  View,
  TextInput,
  Text,
  ScrollView,
  Image,
  Animated,
  TouchableOpacity,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Alert,
  Linking,
  ImageBackground
} from "react-native";
import { Item, Input, Container, Content, Button } from "native-base";
import styles, { IMAGE_HEIGHT, IMAGE_HEIGHT_SMALL } from "./styles";
import { observer } from "mobx-react";
import { Auth } from "../../../Store/Auth";
import Loading from "../../components/loading";
import { LinearGradient, Constants } from "expo";

import logo from "../../../assets/logoLogin.png";
import bgLogin from "../../../assets/bgLogin.png";

@observer
export default class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: ""
    };

    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
    this.OpenForgotPass = this.OpenForgotPass.bind(this);
    this.cadastro = this.cadastro.bind(this);
    this.login = this.login.bind(this);
  }

  OpenForgotPass() {
    Linking.openURL("https://focovirtual.com/user/password/new");
  }

  componentWillMount() {
    if (Platform.OS == "ios") {
      this.keyboardWillShowSub = Keyboard.addListener(
        "keyboardWillShow",
        this.keyboardWillShow
      );
      this.keyboardWillHideSub = Keyboard.addListener(
        "keyboardWillHide",
        this.keyboardWillHide
      );
    } else {
      this.keyboardWillShowSub = Keyboard.addListener(
        "keyboardDidShow",
        this.keyboardDidShow
      );
      this.keyboardWillHideSub = Keyboard.addListener(
        "keyboardDidHide",
        this.keyboardDidHide
      );
    }
  }

  componentWillUnmount() {
    Keyboard.dismiss();
    // this.keyboardWillShowSub.remove();
    // this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = event => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT_SMALL
    }).start();
  };

  keyboardWillHide = event => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT
    }).start();
  };

  keyboardDidShow = event => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT_SMALL
    }).start();
  };

  keyboardDidHide = event => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT
    }).start();
  };

  login() {
    if (this.state.username.length > 0) {
      if (this.state.password.length > 0) {
        Auth.signIn(this.state.username, this.state.password, token => {
          if (token === true) {
            Auth.setLoading(false);
            this.props.navigation.navigate("drawer");
          } else {
            Auth.setLoading(false);
            setTimeout(() => {
              Alert.alert("Atenção", token, [
                { text: "OK", onPress: () => console.log("fechou") }
              ]);
            }, 300);
          }
        });
      } else {
        setTimeout(() => {
          Alert.alert("Atenção", "Insira sua senha.", [
            { text: "OK", onPress: () => console.log("fechou") }
          ]);
        }, 300);
      }
    } else {
      setTimeout(() => {
        Alert.alert("Atenção", "Insira o email de login.", [
          { text: "OK", onPress: () => console.log("fechou") }
        ]);
      }, 300);
    }
  }

  cadastro() {
    this.props.navigation.navigate("SignUp");
  }

  render() {
    return (
      <ImageBackground source={bgLogin} style={{ flex: 1 }}>
        <Loading visible={Auth.loading} />
        <View
          style={{
            flex: 1,
            justifyContent: "space-between"
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "flex-start",
              alignItems: "center",
              paddingTop: 40
              // borderWidth: 1,
              // borderColor: "red"
            }}
          >
            <View
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Animated.Image
                source={logo}
                style={[styles.logo, { height: this.imageHeight }]}
              />
            </View>
            <KeyboardAvoidingView style={styles.container} behavior="padding">
              <Item regular style={styles.iteminput}>
                <Input
                  autoCapitalize="none"
                  placeholder="Email"
                  keyboardType="email-address"
                  style={styles.txtinput}
                  placeholderTextColor="gray"
                  onChangeText={txt => this.setState({ username: txt })}
                  value={this.state.username}
                />
              </Item>
              <Item regular style={styles.iteminput}>
                <Input
                  secureTextEntry
                  placeholder="Senha"
                  style={styles.txtinput}
                  placeholderTextColor="gray"
                  onChangeText={txt => this.setState({ password: txt })}
                  value={this.state.password}
                />
              </Item>
              <TouchableOpacity
                onPress={this.OpenForgotPass}
                style={{
                  alignSelf: "flex-end",
                  margin: 4
                }}
              >
                <Text
                  style={{
                    color: "white"
                  }}
                >
                  Esqueceu sua senha?
                </Text>
              </TouchableOpacity>

              <Button
                full
                bordered
                style={{
                  borderColor: "white",
                  marginTop: 5,
                  borderRadius: 5,
                  marginRight: 3,
                  marginLeft: 2,
                  backgroundColor: "white"
                }}
                onPress={this.login}
              >
                <Text
                  style={{
                    color: "#404040",
                    fontSize: 16,
                    fontWeight: "bold"
                  }}
                >
                  Entrar
                </Text>
              </Button>
            </KeyboardAvoidingView>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              height: "15%"
              // borderWidth: 1,
              // borderColor: "#fff"
            }}
          >
            <Text
              style={{
                color: "white"
              }}
            >
              Não possui acesso?
            </Text>
            <TouchableOpacity onPress={this.cadastro}>
              <Text
                style={{
                  color: "white",
                  marginRight: 2,
                  fontWeight: "bold",
                  marginLeft: 2
                }}
              >
                Cadastre-se
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              // borderWidth: 1,
              // borderColor: "green",
              height: "10%"
            }}
          >
            <Image
              source={require("../../../assets/logoFooter.png")}
              style={{ height: "90%", width: "80%" }}
              resizeMode="contain"
            />
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              // borderWidth: 1,
              // borderColor: "green",
              height: "5%"
            }}
          />
        </View>
      </ImageBackground>
    );
  }
}
