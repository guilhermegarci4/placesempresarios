import React, { Component } from "react";
import { View } from "react-native";
import { createRootNavigator } from "./routers";
import { Auth } from "../../../Store/Auth";

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.renderLayout = this.renderLayout.bind(this);
    this.state = {
      signedIn: false,
      checkedSignedIn: false,
      fontLoaded: false
    };
  }

  componentWillMount() {
    Auth.isSignedIn()
      .then(res => {
        console.log("RES DO MAIN", res);
        this.setState({ signedIn: res, checkedSignedIn: true });
      })
      .catch(err => {
        console.log(err);
        alert("An error has occurred");
      });
  }

  renderLayout() {
    const { checkedSignedIn, signedIn } = this.state;

    if (!checkedSignedIn) {
      return <View />;
    }

    const Layout = createRootNavigator(signedIn);
    return <Layout />;
  }

  render() {
    return this.renderLayout();
  }
}
