import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import SignIn from "../auth/SignIn";
import drawer from "../drawer";
import SignUp from "../auth/SignUp";

export const createRootNavigator = signedIn => {
  return createStackNavigator(
    {
      SignIn: {
        screen: SignIn
      },
      drawer: {
        screen: drawer
      },
      SignUp: {
        screen: SignUp
      }
    },
    {
      initialRouteName: signedIn ? "drawer" : "SignIn",
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    }
  );
};
