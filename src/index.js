import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";

import SignIn from "./screens/auth/SignIn";
import drawer from "./screens/drawer";

const MainNavigator = createStackNavigator(
  {
    SignIn: {
      screen: SignIn
    },
    drawer: {
      screen: drawer
    }
  },
  {
    initialRouteName: "SignIn",
    navigationOptions: {
      header: null
    }
  }
);

export default MainNavigator;
