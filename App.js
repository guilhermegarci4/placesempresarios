import React, { Component } from "react";
import Main from "./src/screens/main/main";
import { Font, AppLoading, Asset } from "expo";
import { SafeAreaView, StyleSheet, View } from "react-native";
import { ifIphoneX } from "react-native-iphone-x-helper";

const bg = require("./assets/bgLogin.png");
const logo = require("./assets/logoLogin.png");
const footer = require("./assets/logoFooter.png");

export default class App extends Component {
  state = {
    isReady: false
  };
  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }

    return (
      <View style={styles.safeArea}>
        <Main />
      </View>
    );
  }

  async _cacheResourcesAsync() {
    const fonts = Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("native-base/Fonts/Ionicons.ttf")
    });

    const images = Images.downloadAsync();

    await Promise.all([fonts, ...images]);
  }
}

class Images {
  static bg = bg;
  static logo = logo;
  static footer = footer;

  static downloadAsync(): Promise<*>[] {
    return [
      Asset.loadAsync(Images.bg),
      Asset.loadAsync(logo),
      Asset.loadAsync(footer)
    ];
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    ...ifIphoneX({ paddingBottom: 25 }, { paddingBottom: 0 })
  }
});
